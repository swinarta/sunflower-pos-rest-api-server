package com.swinarta.sunflower.server.auth;

import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Method;
import org.restlet.security.ChallengeAuthenticator;

public class BasicAuthenticator extends ChallengeAuthenticator{

	public BasicAuthenticator(Context context, ChallengeScheme challengeScheme, String realm) {
		super(context, challengeScheme, realm);
	}
	
	public boolean authenticate(Request request, Response response){
		if(request.getMethod() == Method.GET){
			return true;
		}		
		return super.authenticate(request, response);
	}

}
