package com.swinarta.sunflower.server.auth;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.restlet.representation.Representation;
import org.restlet.service.ConnectorService;

public class HibernateConnectorService extends ConnectorService{
	
	private SessionFactory sessionFactory;
	private Session s = null;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void beforeSend(Representation entity){
		System.out.println("BEFORREEE SENDDDDD");
		s = sessionFactory.openSession();
	}

	public void afterSend(Representation entity){
		System.out.println("AFTER SENDDDDD");
		s.close();
	}
}
