package com.swinarta.sunflower.server.auth;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.routing.Filter;

public class HibernateSessionFilter extends Filter{
	
	private SessionFactory sessionFactory;
	
	private Session s = null;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	protected int beforeHandle(Request request, Response response){
		System.out.println("BEFORE HANDLE");
		s = sessionFactory.openSession();
		return CONTINUE;
	}
	
	protected void afterHandle(Request request, Response response){
		System.out.println("AFTER HANDLE");
		s.close();
	}
	
}
