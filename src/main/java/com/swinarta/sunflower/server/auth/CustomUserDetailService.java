package com.swinarta.sunflower.server.auth;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.swinarta.sunflower.core.manager.CoreManager;

public class CustomUserDetailService implements UserDetailsService{

	private CoreManager coreManager;
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {		
		return coreManager.getUserByUserName(username);
	}

}