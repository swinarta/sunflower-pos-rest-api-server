package com.swinarta.sunflower.server.auth;

import org.restlet.security.LocalVerifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.swinarta.sunflower.core.model.User;

public class SpringSecurityVerifier extends LocalVerifier{

	@Override
	public char[] getLocalSecret(String username) {		
		return "password".toCharArray();
	}
	
	 public boolean verify(String identifier, char[] secret){
		 boolean value =  super.verify(identifier, secret);
		 if(value){
		    	User user = new User();
		    	user.setId(9);
		    	user.setUsername(identifier);

		    	Authentication auth = new UsernamePasswordAuthenticationToken(user,secret);
		    	SecurityContextHolder.getContext().setAuthentication(auth);
		 }
		 return value;
	 }
	
}
