package com.swinarta.sunflower.server.model;

public class DisplayRole {

	private Integer id;
    private String authority;
	
    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}

}
