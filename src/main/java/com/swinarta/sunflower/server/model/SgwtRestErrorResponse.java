package com.swinarta.sunflower.server.model;

import java.util.HashMap;


public class SgwtRestErrorResponse extends SgwtRestResponseBase{
	
	public SgwtRestErrorResponse(Integer status){
		response = new SgwtRestResponse();
		response.setStatus(status);
		response.setErrors(new HashMap<String, String>());
	}
	
	public void addError(String field, String msg){
		response.getErrors().put(field, msg);
	}	

}
