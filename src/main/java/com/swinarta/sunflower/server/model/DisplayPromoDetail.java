package com.swinarta.sunflower.server.model;

public class DisplayPromoDetail extends DisplayBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7176432244098712479L;

	private DisplayProductForPromoDetail product;

	public DisplayProductForPromoDetail getProduct() {
		return product;
	}

	public void setProduct(DisplayProductForPromoDetail product) {
		this.product = product;
	}
	
}
