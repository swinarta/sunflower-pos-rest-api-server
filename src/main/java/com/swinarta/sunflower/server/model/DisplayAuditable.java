package com.swinarta.sunflower.server.model;

import java.util.Date;

public class DisplayAuditable extends DisplayBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2003194271338087803L;

	private Date createdDt;

	private Date updatedDt;
	
	private Integer createdBy;
	
	private Integer updatedBy;
	
	private Integer version;
	
	private Boolean deleteInd;

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Boolean getDeleteInd() {
		return deleteInd;
	}

	public void setDeleteInd(Boolean deleteInd) {
		this.deleteInd = deleteInd;
	}
	
}
