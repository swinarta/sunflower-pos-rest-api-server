package com.swinarta.sunflower.server.model;

import java.util.Set;

public class DisplayLogin extends DisplayBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5265176236874806092L;

	private String username;
	
	private Set<DisplayRole> roles;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Set<DisplayRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<DisplayRole> roles) {
		this.roles = roles;
	}
	
}
