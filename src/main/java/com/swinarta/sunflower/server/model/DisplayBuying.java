package com.swinarta.sunflower.server.model;


public class DisplayBuying extends DisplayAuditable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7216468973118776362L;

	private Double buyingPrice;
	
	private Double disc1;
	
	private Double disc2;
	
	private Double disc3;	
	
	private Double disc4;
		
	private Boolean taxIncluded;
	
	private Double discPrice;
	
	private DisplaySupplier supplier;
	
	private DisplayMeasurement measurement;
	
	public DisplayMeasurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(DisplayMeasurement measurement) {
		this.measurement = measurement;
	}

	public DisplaySupplier getSupplier() {
		return supplier;
	}

	public void setSupplier(DisplaySupplier supplier) {
		this.supplier = supplier;
	}

	public Double getBuyingPrice() {
		return buyingPrice;
	}

	public void setBuyingPrice(Double buyingPrice) {
		this.buyingPrice = buyingPrice;
	}

	public Double getDisc1() {
		return disc1;
	}

	public void setDisc1(Double disc1) {
		this.disc1 = disc1;
	}

	public Double getDisc2() {
		return disc2;
	}

	public void setDisc2(Double disc2) {
		this.disc2 = disc2;
	}

	public Double getDisc3() {
		return disc3;
	}

	public void setDisc3(Double disc3) {
		this.disc3 = disc3;
	}

	public Double getDisc4() {
		return disc4;
	}

	public void setDisc4(Double disc4) {
		this.disc4 = disc4;
	}

	public Boolean getTaxIncluded() {
		return taxIncluded;
	}

	public void setTaxIncluded(Boolean taxIncluded) {
		this.taxIncluded = taxIncluded;
	}

	public Double getDiscPrice() {
		return discPrice;
	}

	public void setDiscPrice(Double discPrice) {
		this.discPrice = discPrice;
	}	

}
