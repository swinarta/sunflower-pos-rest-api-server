package com.swinarta.sunflower.server.model;

import java.io.Serializable;

public class DisplayProductForPromoDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8873826040890210201L;

	private Integer id;
	
	private String barcode;
	
	private String sku;
	
	private Boolean consignment;
	
	private Boolean scallable;
	
	private String longDescription;
	
	private String shortDescription;
		
	private DisplaySelling selling;
		
	private DisplayProductMeasurement productMeasurement;
		
	public DisplayProductMeasurement getProductMeasurement() {
		return productMeasurement;
	}

	public void setProductMeasurement(DisplayProductMeasurement productMeasurement) {
		this.productMeasurement = productMeasurement;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Boolean getConsignment() {
		return consignment;
	}

	public void setConsignment(Boolean consignment) {
		this.consignment = consignment;
	}

	public Boolean getScallable() {
		return scallable;
	}

	public void setScallable(Boolean scallable) {
		this.scallable = scallable;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public DisplaySelling getSelling() {
		return selling;
	}

	public void setSelling(DisplaySelling selling) {
		this.selling = selling;
	}
	

}
