package com.swinarta.sunflower.server.model;

public class DisplayReturnToSupplierDetail extends DisplayBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1762990913803114734L;

	private DisplayProductForReturnToSupplierDetail product;

	private Float qty;
	
	private Double costPrice;
	
	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	public Float getQty() {
		return qty;
	}

	public void setQty(Float qty) {
		this.qty = qty;
	}

	public DisplayProductForReturnToSupplierDetail getProduct() {
		return product;
	}

	public void setProduct(DisplayProductForReturnToSupplierDetail product) {
		this.product = product;
	}
	
}
