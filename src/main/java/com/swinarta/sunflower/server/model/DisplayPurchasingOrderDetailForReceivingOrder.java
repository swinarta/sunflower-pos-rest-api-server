package com.swinarta.sunflower.server.model;

public class DisplayPurchasingOrderDetailForReceivingOrder extends DisplayBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1762990913803114734L;

	private DisplayProductWithoutStockForPurchasingOrderDetail product;

	private Float qty;
	
	private Double costPrice;
	
	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	public Float getQty() {
		return qty;
	}

	public void setQty(Float qty) {
		this.qty = qty;
	}

	public DisplayProductWithoutStockForPurchasingOrderDetail getProduct() {
		return product;
	}

	public void setProduct(DisplayProductWithoutStockForPurchasingOrderDetail product) {
		this.product = product;
	}
	
}
