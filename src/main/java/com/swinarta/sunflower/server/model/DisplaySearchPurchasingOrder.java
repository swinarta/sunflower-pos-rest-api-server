package com.swinarta.sunflower.server.model;

import java.util.Date;

import com.swinarta.sunflower.core.model.PurchasingOrder.Status;

public class DisplaySearchPurchasingOrder extends DisplayAuditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7294232685855182124L;

	private String poId;
	
	private Date poDate;
	
	private String remarks;
	
	private Date cancelDate;

	private Status status;

	private DisplaySupplier supplier;

	public String getPoId() {
		return poId;
	}

	public void setPoId(String poId) {
		this.poId = poId;
	}

	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public DisplaySupplier getSupplier() {
		return supplier;
	}

	public void setSupplier(DisplaySupplier supplier) {
		this.supplier = supplier;
	}
}
