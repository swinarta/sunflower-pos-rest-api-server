package com.swinarta.sunflower.server.model;

import java.util.Date;

import com.swinarta.sunflower.core.model.ReturnToSupplier.Status;

public class DisplaySearchReturnToSupplier extends DisplayAuditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7294232685855182124L;

	private String returnId;
	
	private Date returnDate;
	
	private String remarks;

	private String invoiceNumber;

	private Status status;

	private DisplaySupplier supplier;

	public String getReturnId() {
		return returnId;
	}

	public void setReturnId(String returnId) {
		this.returnId = returnId;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public DisplaySupplier getSupplier() {
		return supplier;
	}

	public void setSupplier(DisplaySupplier supplier) {
		this.supplier = supplier;
	}

	
}
