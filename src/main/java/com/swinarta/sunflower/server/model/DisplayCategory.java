package com.swinarta.sunflower.server.model;

import java.io.Serializable;

public class DisplayCategory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7460921904743942641L;

	private Integer id;
	
	private String description;

	private DisplayCategory mainCategory;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DisplayCategory getMainCategory() {
		if(mainCategory != null && mainCategory.getId() == 0) return null;
		return mainCategory;
	}

	public void setMainCategory(DisplayCategory mainCategory) {
		this.mainCategory = mainCategory;
	}
		
}