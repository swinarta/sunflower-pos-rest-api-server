package com.swinarta.sunflower.server.model;


public class DisplayProductMeasurement extends DisplayAuditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1889265144293877620L;

	private DisplayMeasurement measurement;
	
	private Integer overrideQty;

	public DisplayMeasurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(DisplayMeasurement measurement) {
		this.measurement = measurement;
	}

	public Integer getOverrideQty() {
		return overrideQty;
	}

	public void setOverrideQty(Integer overrideQty) {
		this.overrideQty = overrideQty;
	}		

}