package com.swinarta.sunflower.server.model;

public abstract class SgwtRestResponseBase {

	public static Integer RESPONSE_FAILURE = -1;
	public static Integer RESPONSE_SUCCESS = 0;
	
	protected SgwtRestResponse response;

	public SgwtRestResponse getResponse() {
		return response;
	}

	public void setResponse(SgwtRestResponse response) {
		this.response = response;
	}

}
