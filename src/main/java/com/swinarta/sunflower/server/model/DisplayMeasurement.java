package com.swinarta.sunflower.server.model;


public class DisplayMeasurement extends DisplayAuditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -752407967632351245L;

	private String description;

	private String code;
	
	private Integer defaultQty;
	
	private Boolean mutable;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getDefaultQty() {
		return defaultQty;
	}

	public void setDefaultQty(Integer defaultQty) {
		this.defaultQty = defaultQty;
	}

	public Boolean getMutable() {
		return mutable;
	}

	public void setMutable(Boolean mutable) {
		this.mutable = mutable;
	}	
	
}
