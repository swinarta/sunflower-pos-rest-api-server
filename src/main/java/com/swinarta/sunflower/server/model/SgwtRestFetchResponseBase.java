package com.swinarta.sunflower.server.model;

import java.io.Serializable;
import java.util.Collection;

import com.swinarta.sunflower.core.data.ResultList;

public class SgwtRestFetchResponseBase extends SgwtRestResponseBase{

	public SgwtRestFetchResponseBase(Collection result){
		response = new SgwtRestResponse();
		response.setStartRow(0);
		response.setStatus(0);
		response.setTotalRows(result.size());
		response.setData(result.toArray());

		response.setEndRow(result.size()-1);
	}

	
	@SuppressWarnings("unchecked")
	public SgwtRestFetchResponseBase(ResultList result){
		response = new SgwtRestResponse();
		response.setStartRow(result.getStartRow());
		response.setStatus(0);
		response.setTotalRows(result.getTotalCount());
		response.setData(result.toArray());

		if(result.getTotalCount() < result.getEndRow() ){
			response.setEndRow(result.getTotalCount());
		}else{
			response.setEndRow(result.getEndRow());
		}
	}
	
	public SgwtRestFetchResponseBase(Serializable object){
		response = new SgwtRestResponse();
		response.setStartRow(0);
		response.setStatus(0);
		
		if(object == null){
			response.setTotalRows(0);			
			response.setEndRow(0);
		}else{
			response.setTotalRows(1);
			response.setData(new Serializable[] {object});
			response.setEndRow(1);		
		}
	}
	
	public void setStatus(Integer status){
		response.setStatus(status);
	}
	
	public void addError(String field, String msg){
		response.getErrors().put(field, msg);
	}
	
}
