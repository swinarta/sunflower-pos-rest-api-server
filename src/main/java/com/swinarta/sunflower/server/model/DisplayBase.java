package com.swinarta.sunflower.server.model;

import java.io.Serializable;

public abstract class DisplayBase implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1361660756514149647L;
	
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
