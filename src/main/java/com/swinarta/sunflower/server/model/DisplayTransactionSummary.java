package com.swinarta.sunflower.server.model;

import java.io.Serializable;
import java.util.Date;

public class DisplayTransactionSummary implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3688823691413463115L;
	
	private Integer stationId;
	
	private Date transactionDate;
	
	private Long total;
		
	private String paymentType;
	
	private Long count;
	
	private String userName;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Integer getStationId() {
		return stationId;
	}

	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}


}
