package com.swinarta.sunflower.server.model;

import java.util.Date;

import com.swinarta.sunflower.core.model.ReceivingOrder.Status;

public class DisplayReceivingOrder extends DisplayBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8304443232886690854L;

	private String roId;

	private Date roDate;

	private String remarks;
	
	private Status status;
	
	private DisplaySearchPurchasingOrder po;
	
	public DisplaySearchPurchasingOrder getPo() {
		return po;
	}

	public void setPo(DisplaySearchPurchasingOrder po) {
		this.po = po;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getRoId() {
		return roId;
	}

	public void setRoId(String roId) {
		this.roId = roId;
	}

	public Date getRoDate() {
		return roDate;
	}

	public void setRoDate(Date roDate) {
		this.roDate = roDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
