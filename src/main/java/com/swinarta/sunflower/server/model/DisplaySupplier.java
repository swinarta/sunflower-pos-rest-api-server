package com.swinarta.sunflower.server.model;



public class DisplaySupplier extends DisplayBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7270026895891657427L;

	private String name;
	
	private String supplierCode;
	
	private String address1;

	private String address2;
	
	private String city;

	private String phone;
	
	private String fax;
	
	private String mobile;

	private String contactName;
	
	private Integer termOfPayment;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public Integer getTermOfPayment() {
		return termOfPayment;
	}

	public void setTermOfPayment(Integer termOfPayment) {
		this.termOfPayment = termOfPayment;
	}	
	
}
