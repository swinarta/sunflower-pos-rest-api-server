package com.swinarta.sunflower.server.model;

import com.swinarta.sunflower.core.model.Stock.StockUpdateReason;


public class DisplayStock extends DisplayAuditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9074418699148719779L;

	private Integer max;

	private Integer min;

	private Integer defaultOrder;

	private Float current;

	private Integer order = 0;

	private Integer storeId;
	
	private StockUpdateReason lastUpdateReason;
	
	private Integer productId;
	
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public Integer getMin() {
		return min;
	}

	public void setMin(Integer min) {
		this.min = min;
	}

	public Integer getDefaultOrder() {
		return defaultOrder;
	}

	public void setDefaultOrder(Integer defaultOrder) {
		this.defaultOrder = defaultOrder;
	}

	public Float getCurrent() {
		return current;
	}

	public void setCurrent(Float current) {
		this.current = current;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public StockUpdateReason getLastUpdateReason() {
		return lastUpdateReason;
	}

	public void setLastUpdateReason(StockUpdateReason lastUpdateReason) {
		this.lastUpdateReason = lastUpdateReason;
	}
	
}
