package com.swinarta.sunflower.server.model;

import java.util.Date;

import com.swinarta.sunflower.core.model.TransferOrder.Status;

public class DisplayTransferOrder extends DisplayAuditable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 2514466826719113511L;

	private String transferId;
	
	private Date transferDate;
	
	private String remarks;
	
	private Status status;
	
	private String fromStoreCode;
	
	private String toStoreCode;
	
	public String getFromStoreCode() {
		return fromStoreCode;
	}

	public void setFromStoreCode(String fromStoreCode) {
		this.fromStoreCode = fromStoreCode;
	}

	public String getToStoreCode() {
		return toStoreCode;
	}

	public void setToStoreCode(String toStoreCode) {
		this.toStoreCode = toStoreCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getTransferId() {
		return transferId;
	}

	public void setTransferId(String transferId) {
		this.transferId = transferId;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

}
