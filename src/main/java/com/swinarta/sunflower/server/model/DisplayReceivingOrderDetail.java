package com.swinarta.sunflower.server.model;

public class DisplayReceivingOrderDetail extends DisplayBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1762990913803114734L;

	private DisplayPurchasingOrderDetailForReceivingOrder poDetail;
	
	private Float qty;
	
	private Double costPrice;
	
	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	public DisplayPurchasingOrderDetailForReceivingOrder getPoDetail() {
		return poDetail;
	}

	public void setPoDetail(DisplayPurchasingOrderDetailForReceivingOrder poDetail) {
		this.poDetail = poDetail;
	}

	public Float getQty() {
		return qty;
	}

	public void setQty(Float qty) {
		this.qty = qty;
	}
	
	
}
