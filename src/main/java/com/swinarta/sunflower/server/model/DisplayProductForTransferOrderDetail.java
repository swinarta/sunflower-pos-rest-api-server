package com.swinarta.sunflower.server.model;

import java.io.Serializable;

public class DisplayProductForTransferOrderDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8873826040890210201L;

	private Integer id;
	
	private String barcode;
	
	private String sku;
	
	private String longDescription;
	
	private String shortDescription;
			
	private DisplayProductMeasurement productMeasurement;
	
	private DisplayBuying buying;
		
	public DisplayBuying getBuying() {
		return buying;
	}

	public void setBuying(DisplayBuying buying) {
		this.buying = buying;
	}

	public DisplayProductMeasurement getProductMeasurement() {
		return productMeasurement;
	}

	public void setProductMeasurement(DisplayProductMeasurement productMeasurement) {
		this.productMeasurement = productMeasurement;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

}
