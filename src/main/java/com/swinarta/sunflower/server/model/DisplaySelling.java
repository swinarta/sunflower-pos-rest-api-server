package com.swinarta.sunflower.server.model;


public class DisplaySelling extends DisplayAuditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5657799213473773596L;

	private Integer id;

	private Double sellingPrice;
	
	private DisplayMeasurement measurement;
	
	public DisplayMeasurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(DisplayMeasurement measurement) {
		this.measurement = measurement;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

}