package com.swinarta.sunflower.server.model;

import java.util.Date;

import com.swinarta.sunflower.core.model.Promo.PromoType;

public class DisplayPromo extends DisplayBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2126403628335937932L;

	private Date startDate;
	
	private Date endDate;
	
	private PromoType promoType;
	
	private Double promoValue;

	private Integer qty = 1;

	private String description;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public PromoType getPromoType() {
		return promoType;
	}

	public void setPromoType(PromoType promoType) {
		this.promoType = promoType;
	}

	public Double getPromoValue() {
		return promoValue;
	}

	public void setPromoValue(Double promoValue) {
		this.promoValue = promoValue;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
