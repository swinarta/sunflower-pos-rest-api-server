package com.swinarta.sunflower.server.model;

import java.util.HashMap;
import java.util.Map;

public class SgwtRestDeleteResponse extends SgwtRestResponseBase{

	public SgwtRestDeleteResponse(int id){
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("id", id);
		Object[] data = new Object[1];
		data[0] = map;
		
		this.response = new SgwtRestResponse();
		response.setData(data);
		response.setStatus(RESPONSE_SUCCESS);
	}
	
}
