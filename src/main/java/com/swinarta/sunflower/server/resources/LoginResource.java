package com.swinarta.sunflower.server.resources;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.swinarta.sunflower.core.model.User;
import com.swinarta.sunflower.server.model.DisplayLogin;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;

public class LoginResource extends ServerResource{

	private Mapper mapper;
		
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	@Get("json")
	public SgwtRestFetchResponseBase represent(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User actor = (User)auth.getPrincipal();
    	
    	DisplayLogin disp = mapper.map(actor, DisplayLogin.class);
    	
    	SgwtRestFetchResponseBase resp = new SgwtRestFetchResponseBase(disp);
		
		return resp;
    	
	}

}
