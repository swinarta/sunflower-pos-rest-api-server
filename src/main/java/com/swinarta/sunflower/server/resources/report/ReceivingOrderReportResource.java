package com.swinarta.sunflower.server.resources.report;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;

import org.restlet.data.MediaType;
import org.restlet.representation.FileRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ReceivingOrderDetail;
import com.swinarta.sunflower.core.model.Store;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ReceivingOrderReportResource extends ServerResource{

	protected CoreManager coreManager;
	
	protected JRTextExporter jtextExporter;

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}	
	
	public void setJtextExporter(JRTextExporter jtextExporter) {
		this.jtextExporter = jtextExporter;
	}

	@Get
	public FileRepresentation getRepresentation(){		
		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		String type = RequestUtil.getString(getRequestAttributes().get("type"));

		File respFile = null;
		FileRepresentation output = null;

		InputStream jasperFileStream = getClass().getResourceAsStream("/report/roreport.jasper");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		
		List<ReceivingOrderDetail> list = coreManager.getReceivingOrderDetails(id);
		
		if(list.size() > 0){
			Integer storeId = list.get(0).getReceivingOrder().getPo().getDeliverStoreId();
			Store store = coreManager.get(Store.class, storeId);
			paramMap.put("storeName", store.getName());
			paramMap.put("storeAddress", store.getAddress());
			paramMap.put("storeCity", store.getCity());
		}
				
		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(list);
		
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperFileStream, paramMap, ds);
			if("pdf".equalsIgnoreCase(type)){
				respFile = File.createTempFile("sunflower_", ".pdf");
				JasperExportManager.exportReportToPdfFile(jasperPrint, respFile.getPath());
			}else{
				respFile = File.createTempFile("sunflower_", ".txt");	
				jtextExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				jtextExporter.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT, new Float(10));
				jtextExporter.setParameter(JRTextExporterParameter.CHARACTER_WIDTH, new Float(5.25));
				jtextExporter.setParameter(JRExporterParameter.OUTPUT_FILE, respFile);
				jtextExporter.exportReport();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jasperFileStream != null){
				try {
					jasperFileStream.close();
				} catch (IOException e) {
				}
			}
		}

		if("pdf".equalsIgnoreCase(type)){
			output = new FileRepresentation(respFile, MediaType.APPLICATION_PDF);
		}else{
			output = new FileRepresentation(respFile, MediaType.TEXT_PLAIN);
		}
		return output;		
	}
	
}
