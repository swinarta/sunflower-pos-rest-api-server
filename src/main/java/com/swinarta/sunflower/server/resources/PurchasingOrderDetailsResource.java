package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.PurchasingOrder;
import com.swinarta.sunflower.core.model.PurchasingOrder.Status;
import com.swinarta.sunflower.core.model.PurchasingOrderDetail;
import com.swinarta.sunflower.core.model.Stock;

import com.swinarta.sunflower.server.model.DisplayProductMeasurement;
import com.swinarta.sunflower.server.model.DisplayPurchasingOrderDetail;
import com.swinarta.sunflower.server.model.DisplayStock;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class PurchasingOrderDetailsResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		SgwtRestFetchResponseBase resp = null;
		Integer poId = RequestUtil.getInteger(getQuery().getValues("poId"));
		
		List<PurchasingOrderDetail> details = coreManager.getPurchasingOrderDetails(poId);
		
		List<DisplayPurchasingOrderDetail> resultList = new ArrayList<DisplayPurchasingOrderDetail>();
		
		for (PurchasingOrderDetail purchasingOrderDetail : details) {
			
			Stock stock = null;
			ProductMeasurement productMeasurement = null;
			
			if(purchasingOrderDetail.getProduct().getStock() != null && !purchasingOrderDetail.getProduct().getStock().isEmpty()){
				stock = purchasingOrderDetail.getProduct().getStock().iterator().next();
			}
			if(purchasingOrderDetail.getProduct().getProductMeasurement() != null && !purchasingOrderDetail.getProduct().getProductMeasurement().isEmpty()){
				productMeasurement = purchasingOrderDetail.getProduct().getProductMeasurement().iterator().next();
			}
						
			DisplayPurchasingOrderDetail det = mapper.map(purchasingOrderDetail, DisplayPurchasingOrderDetail.class);
			
			if(stock != null){
				det.getProduct().setStock(mapper.map(stock, DisplayStock.class));
			}
			
			if(productMeasurement != null){
				det.getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
			}

			if(purchasingOrderDetail.getPurchasingOrder().getStatus().equals(Status.COMPLETED)){
				det.setCostPrice(purchasingOrderDetail.getCostPriceOnCompleted());
			}else{
				det.setCostPrice(purchasingOrderDetail.getProduct().getBuying().getCostPrice());
			}
			
			resultList.add(det);
		}
		
		resp = new SgwtRestFetchResponseBase(resultList);
		
		return resp;

	}
	
	@Post("json")
	public SgwtRestResponseBase add(SgwtRequest request){
		Float qty = RequestUtil.getFloat(request.getData().get("qty"));
		Integer poId = RequestUtil.getInteger(request.getData().get("poId"));
		Integer productId = RequestUtil.getInteger(request.getData().get("productId"));
		Serializable resp = null;
		
		try{
			Product product = coreManager.get(Product.class, productId);
			PurchasingOrder po = coreManager.get(PurchasingOrder.class, poId);
			
			PurchasingOrderDetail pod = new PurchasingOrderDetail();
			pod.setProduct(product);
			pod.setPurchasingOrder(po);
			pod.setQty(qty);
			
			PurchasingOrderDetail podResp = coreManager.save(PurchasingOrderDetail.class, pod);
			podResp = coreManager.getPurchasingOrderDetail(pod.getId());
			DisplayPurchasingOrderDetail det =  mapper.map(podResp, DisplayPurchasingOrderDetail.class);
			det.setCostPrice(podResp.getProduct().getBuying().getCostPrice());
			
			Stock stock = null;
			ProductMeasurement productMeasurement = null;			
			if(podResp.getProduct().getStock() != null && !podResp.getProduct().getStock().isEmpty()){
				stock = podResp.getProduct().getStock().iterator().next();
			}
			if(podResp.getProduct().getProductMeasurement() != null && !podResp.getProduct().getProductMeasurement().isEmpty()){
				productMeasurement = podResp.getProduct().getProductMeasurement().iterator().next();
			}
			if(stock != null){
				det.getProduct().setStock(mapper.map(stock, DisplayStock.class));
			}
			
			if(productMeasurement != null){
				det.getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
			}			
			
			resp = det;			
			
		}catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}

		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		
		return ret;
	
	}
}
