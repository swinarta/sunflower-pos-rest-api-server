package com.swinarta.sunflower.server.resources;

import java.util.Date;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Promo;
import com.swinarta.sunflower.server.model.DisplayPromo;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class CheckPromoResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		
		Integer productId = RequestUtil.getInteger(getRequestAttributes().get("productId"));
		
		Date startDate = RequestUtil.getDate(getRequestAttributes().get("startDate"));
		Date endDate = RequestUtil.getDate(getRequestAttributes().get("endDate"));

		Promo promo = coreManager.getPromo(productId, startDate, endDate);
		DisplayPromo disp = null;
		
		if(promo != null){
			disp = mapper.map(promo, DisplayPromo.class);
		}
		
		SgwtRestFetchResponseBase resp = new SgwtRestFetchResponseBase(disp);
		
		return resp;
	}
	
}
