package com.swinarta.sunflower.server.resources;

import java.io.Serializable;

import org.dozer.Mapper;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.ReceivingOrderDetail;
import com.swinarta.sunflower.server.model.DisplayProductMeasurement;
import com.swinarta.sunflower.server.model.DisplayReceivingOrderDetail;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRequest.OPERATION_TYPE;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ReceivingOrderDetailResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Post("json")
	public SgwtRestResponseBase action(SgwtRequest request){
		Serializable resp = null;

		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		
		if(request.getOperationType() == OPERATION_TYPE.UPDATE){
			Float qty = RequestUtil.getFloat(request.getData().get("qty"));
			
			ReceivingOrderDetail rod = coreManager.get(ReceivingOrderDetail.class, id);
			rod.setQty(qty);
			
			try {
				ReceivingOrderDetail rodResp = coreManager.save(ReceivingOrderDetail.class, rod);
				rodResp = coreManager.getReceivingOrderDetail(rodResp.getId());
				DisplayReceivingOrderDetail det =  mapper.map(rodResp, DisplayReceivingOrderDetail.class);
				det.setCostPrice(rodResp.getPoDetail().getProduct().getBuying().getCostPrice());

				ProductMeasurement productMeasurement = null;			
				if(rodResp.getPoDetail().getProduct().getProductMeasurement() != null && !rodResp.getPoDetail().getProduct().getProductMeasurement().isEmpty()){
					productMeasurement = rodResp.getPoDetail().getProduct().getProductMeasurement().iterator().next();
				}
				
				if(productMeasurement != null){
					det.getPoDetail().getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
				}			
				
				resp = det;
				
			} catch (Exception e) {
				SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
				resp1.addError("exception", e.getMessage());
				return resp1;				
			}			
		}		
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		
		return ret;
		

	}
}
