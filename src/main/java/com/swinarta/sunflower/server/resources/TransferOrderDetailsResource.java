package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.TransferOrder;

import com.swinarta.sunflower.core.model.TransferOrderDetail;
import com.swinarta.sunflower.server.model.DisplayProductMeasurement;
import com.swinarta.sunflower.server.model.DisplayTransferOrderDetail;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class TransferOrderDetailsResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		SgwtRestFetchResponseBase resp = null;
		Integer transferId = RequestUtil.getInteger(getRequestAttributes().get("transferId"));
		
		List<TransferOrderDetail> details = coreManager.getTransferOrderDetails(transferId);
		
		List<DisplayTransferOrderDetail> resultList = new ArrayList<DisplayTransferOrderDetail>();
		
		for (TransferOrderDetail transferOrderDetail : details) {
			
			ProductMeasurement productMeasurement = null;
			
			if(transferOrderDetail.getProduct().getProductMeasurement() != null && !transferOrderDetail.getProduct().getProductMeasurement().isEmpty()){
				productMeasurement = transferOrderDetail.getProduct().getProductMeasurement().iterator().next();
			}
						
			DisplayTransferOrderDetail det = mapper.map(transferOrderDetail, DisplayTransferOrderDetail.class);
						
			if(productMeasurement != null){
				det.getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
			}
			
			resultList.add(det);
		}
		
		resp = new SgwtRestFetchResponseBase(resultList);
		
		return resp;

	}
	
	@Post("json")
	public SgwtRestResponseBase add(SgwtRequest request){
		Float qty = RequestUtil.getFloat(request.getData().get("qty"));
		Integer toId = RequestUtil.getInteger(request.getData().get("transferId"));
		Integer productId = RequestUtil.getInteger(request.getData().get("productId"));
		Serializable resp = null;
		
		try{
			Product product = coreManager.get(Product.class, productId);
			TransferOrder to = coreManager.get(TransferOrder.class, toId);
			
			TransferOrderDetail tod = new TransferOrderDetail();
			tod.setProduct(product);
			tod.setTransferOrder(to);
			tod.setQty(qty);
			
			TransferOrderDetail todResp = coreManager.save(TransferOrderDetail.class, tod);
			todResp = coreManager.getTransferOrderDetail(tod.getId());
			DisplayTransferOrderDetail det =  mapper.map(todResp, DisplayTransferOrderDetail.class);
			
			ProductMeasurement productMeasurement = null;			
			if(todResp.getProduct().getProductMeasurement() != null && !todResp.getProduct().getProductMeasurement().isEmpty()){
				productMeasurement = todResp.getProduct().getProductMeasurement().iterator().next();
			}
			
			if(productMeasurement != null){
				det.getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
			}			
			
			resp = det;
			
		}catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}

		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		
		return ret;
	
	}
}
