package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.PurchasingOrder;
import com.swinarta.sunflower.core.model.PurchasingOrder.Status;
import com.swinarta.sunflower.core.model.Supplier;
import com.swinarta.sunflower.server.model.DisplaySearchPurchasingOrder;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.PoUtil;
import com.swinarta.sunflower.server.util.RequestUtil;

public class PurchasingOrdersResource extends ServerResource{

	private CoreManager coreManager;
	
	private Mapper mapper;
	
	private Properties properties;
	
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}
	
	@Get("json")
	public SgwtRestFetchResponseBase getRepresent(){
		SgwtRestFetchResponseBase resp = null;
		String text = RequestUtil.getString(getQuery().getValues("searchPO"));
		String statusStr = RequestUtil.getString(getQuery().getValues("poStatusStr"));
		Integer supplierId = RequestUtil.getInteger(getQuery().getValues("supplierId"));		
		Integer start = RequestUtil.getInteger(getQuery().getValues("_startRow"));
		Integer end = RequestUtil.getInteger(getQuery().getValues("_endRow"));
		
		Status status = Status.fromString(statusStr);

		ResultList<PurchasingOrder> list = coreManager.searchPurchasingOrder(text, supplierId, status, start, end);
		
		//ResultList<DisplaySearchPurchasingOrder> list2 = new ResultList<DisplaySearchPurchasingOrder>(new ArrayList<DisplaySearchPurchasingOrder>());
		
		//mapper.map(list, list2);
		
		//System.out.println(list2);
		
		List<DisplaySearchPurchasingOrder> list2 = new ArrayList<DisplaySearchPurchasingOrder>();
		
		for (PurchasingOrder purchasingOrder : list) {
			list2.add(mapper.map(purchasingOrder, DisplaySearchPurchasingOrder.class));
		}
		
		ResultList<DisplaySearchPurchasingOrder> list3 = new ResultList<DisplaySearchPurchasingOrder>(list2);
		list3.setStartRow(list.getStartRow());
		list3.setEndRow(list.getEndRow());
		list3.setTotalCount(list.getTotalCount());
		
		//resp = new SgwtRestFetchResponseBase(new ResultList<PurchasingOrder>(list, PurchasingOrder.class));
		resp = new SgwtRestFetchResponseBase(new ResultList<DisplaySearchPurchasingOrder>(list3, DisplaySearchPurchasingOrder.class));
		return resp;
	}
	
	@Post("json")
	public SgwtRestResponseBase add(SgwtRequest req){
		Serializable resp = null;
		Date poDate = RequestUtil.getDate(req.getData().get("poDate")); 
		Date cancelDate = RequestUtil.getDate(req.getData().get("cancelDate"));
		Integer supplierId = RequestUtil.getInteger(req.getData().get("supplierId"));
		String remarks = RequestUtil.getString(req.getData().get("remarks"));
		
		Supplier supplier = coreManager.get(Supplier.class, supplierId);
		
		Integer storeId = Integer.parseInt(properties.getProperty("curr.store.id"));
		
		PurchasingOrder po = new PurchasingOrder();
		po.setCancelDate(cancelDate);
		po.setPoDate(poDate);
		po.setSupplier(supplier);
		po.setDeliverStoreId(storeId);
		po.setRemarks(remarks);
		po.setStatus(Status.NEW);
		po.setPoId("temp");
		
		try {
			PurchasingOrder poResp = coreManager.save(PurchasingOrder.class, po);
			poResp.setPoId(PoUtil.constructPoId(poResp.getPoDate(), poResp.getSupplier().getId(), poResp.getId()));
			poResp = coreManager.save(PurchasingOrder.class, poResp);
			
			DisplaySearchPurchasingOrder dpo = mapper.map(poResp, DisplaySearchPurchasingOrder.class);
			resp = dpo;
			
		} catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;
	}
}
