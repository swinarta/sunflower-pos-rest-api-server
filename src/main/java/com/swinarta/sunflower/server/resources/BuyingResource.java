package com.swinarta.sunflower.server.resources;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Buying;

public class BuyingResource extends ServerResource{

	protected CoreManager coreManager;

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}			

	@Get("json")
	public Buying represent(){
		
		String idStr = (String) getRequest().getAttributes().get("id");
		Integer id = Integer.parseInt(idStr);			
		return coreManager.get(Buying.class, id);
		
	}
	
}
