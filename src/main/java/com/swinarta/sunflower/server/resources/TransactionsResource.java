package com.swinarta.sunflower.server.resources;

import java.util.Date;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Transaction;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class TransactionsResource extends ServerResource {
	protected CoreManager coreManager;

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		String code = RequestUtil.getString(getQuery().getValues("code"));
		Integer stationId = RequestUtil.getInteger(getQuery().getValues("stationId"));
		Date date = RequestUtil.getDate(getRequestAttributes().get("date"));
		Integer start = RequestUtil.getInteger(getQuery().getValues("_startRow"));
		Integer end = RequestUtil.getInteger(getQuery().getValues("_endRow"));
		
		ResultList<Transaction> displayResultList = coreManager.searchTransaction(date, stationId, code, start, end);
		
		return new SgwtRestFetchResponseBase(displayResultList);		
	}
		
}
