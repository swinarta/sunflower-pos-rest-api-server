package com.swinarta.sunflower.server.resources;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.springframework.dao.DataIntegrityViolationException;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Supplier;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class SupplierResource extends ServerResource{

	private CoreManager coreManager;

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Post("json")
	public SgwtRestResponseBase post(SgwtRequest request){

		Serializable resp = null;

		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		String supplierCode = RequestUtil.getString(request.getData().get("supplierCode"));
		String supplierName = RequestUtil.getString(request.getData().get("name"));
		String contactName = RequestUtil.getString(request.getData().get("contactName"));
		String addr1 = RequestUtil.getString(request.getData().get("address1"));
		String addr2 = RequestUtil.getString(request.getData().get("address2"));
		String city = RequestUtil.getString(request.getData().get("city"));
		String phone = RequestUtil.getString(request.getData().get("phone"));
		String fax = RequestUtil.getString(request.getData().get("fax"));
		String mobile = RequestUtil.getString(request.getData().get("mobile"));
		Integer termOfPayment = RequestUtil.getInteger(request.getData().get("termOfPayment"));
	
		Supplier supp = coreManager.get(Supplier.class, id);
		supp.setSupplierCode(supplierCode);
		supp.setName(supplierName);
		supp.setContactName(contactName);
		supp.setAddress1(addr1);
		supp.setAddress2(addr2);
		supp.setCity(city);
		supp.setPhone(phone);
		supp.setFax(fax);
		supp.setMobile(mobile);
		supp.setTermOfPayment(termOfPayment);
	
		try {
			resp = coreManager.save(Supplier.class, supp);
		}catch (DataIntegrityViolationException dive){

			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-4);

			String contraintMsg = dive.getCause().getCause().getMessage();
			if(StringUtils.indexOf(contraintMsg, "supplier_id") > 0){
				resp1.addError("supplierCode", "Supplier Code Found");
			}				
			return resp1;				
		} catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}

		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
	
		return ret;

	}	
}
