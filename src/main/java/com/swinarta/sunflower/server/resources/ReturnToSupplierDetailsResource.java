package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.ReturnToSupplier;
import com.swinarta.sunflower.core.model.ReturnToSupplier.Status;
import com.swinarta.sunflower.core.model.ReturnToSupplierDetail;
import com.swinarta.sunflower.server.model.DisplayProductMeasurement;
import com.swinarta.sunflower.server.model.DisplayReturnToSupplierDetail;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ReturnToSupplierDetailsResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		SgwtRestFetchResponseBase resp = null;
		Integer retId = RequestUtil.getInteger(getQuery().getValues("retId"));
		
		List<ReturnToSupplierDetail> details = coreManager.getReturnToSupplierDetails(retId);
		
		List<DisplayReturnToSupplierDetail> resultList = new ArrayList<DisplayReturnToSupplierDetail>();
		
		for (ReturnToSupplierDetail retDetail : details) {
			
			ProductMeasurement productMeasurement = null;
			
			if(retDetail.getProduct().getProductMeasurement() != null && !retDetail.getProduct().getProductMeasurement().isEmpty()){
				productMeasurement = retDetail.getProduct().getProductMeasurement().iterator().next();
			}
						
			DisplayReturnToSupplierDetail det = mapper.map(retDetail, DisplayReturnToSupplierDetail.class);
			if(retDetail.getReturnToSupplier().getStatus().equals(Status.COMPLETED)){
				det.setCostPrice(retDetail.getCostPriceOnCompleted());
			}else{
				det.setCostPrice(retDetail.getProduct().getBuying().getCostPrice());
			}
						
			if(productMeasurement != null){
				det.getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
			}
						
			resultList.add(det);
		}
		
		resp = new SgwtRestFetchResponseBase(resultList);
		
		return resp;

	}
	
	@Post("json")
	public SgwtRestResponseBase add(SgwtRequest request){
		Float qty = RequestUtil.getFloat(request.getData().get("qty"));
		Integer retId = RequestUtil.getInteger(request.getData().get("retId"));
		Integer productId = RequestUtil.getInteger(request.getData().get("productId"));
		Serializable resp = null;
		
		try{
			Product product = coreManager.get(Product.class, productId);
			ReturnToSupplier ret = coreManager.get(ReturnToSupplier.class, retId);
			
			ReturnToSupplierDetail retd = new ReturnToSupplierDetail();
			retd.setProduct(product);
			retd.setReturnToSupplier(ret);
			retd.setQty(qty);
			
			ReturnToSupplierDetail retResp = coreManager.save(ReturnToSupplierDetail.class, retd);
			retResp = coreManager.getReturnToSupplierDetail(retd.getId());
			DisplayReturnToSupplierDetail det =  mapper.map(retResp, DisplayReturnToSupplierDetail.class);
			det.setCostPrice(retResp.getProduct().getBuying().getCostPrice());
			
			ProductMeasurement productMeasurement = null;			
			if(retResp.getProduct().getProductMeasurement() != null && !retResp.getProduct().getProductMeasurement().isEmpty()){
				productMeasurement = retResp.getProduct().getProductMeasurement().iterator().next();
			}
			
			if(productMeasurement != null){
				det.getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
			}			
			
			resp = det;			
			
		}catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}

		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		
		return ret;
	
	}
}
