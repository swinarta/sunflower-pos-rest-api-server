package com.swinarta.sunflower.server.resources.ds;

import org.dozer.Mapper;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.springframework.orm.ObjectRetrievalFailureException;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Measurement;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.core.model.Selling;
import com.swinarta.sunflower.server.model.DisplaySelling;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.model.SgwtRequest.OPERATION_TYPE;
import com.swinarta.sunflower.server.util.RequestUtil;

public class SellingResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}	

	@Post("json")
	public SgwtRestResponseBase getRepresent(SgwtRequest request){
		
		Selling respSelling = null;
		Integer id = (Integer) request.getData().get("id");
		
		if(request.getOperationType() == OPERATION_TYPE.FETCH){			

			try{
				respSelling = coreManager.get(Selling.class, id);
			}catch (ObjectRetrievalFailureException e) {
			}
		
		}else if(request.getOperationType() == OPERATION_TYPE.UPDATE){
			Double sellingPrice = ((Number) request.getData().get("sellingPrice")).doubleValue();
			Integer measurementSelectionId = Integer.parseInt((String) request.getData().get("measurementSelectionId"));
			
			Selling s = coreManager.get(Selling.class, id);
			s.setSellingPrice(sellingPrice);
			
			if(measurementSelectionId.intValue() != s.getMeasurement().getId().intValue()){
				Measurement selm = coreManager.get(Measurement.class, measurementSelectionId);
				s.setMeasurement(selm);
			}
			
			try{
				respSelling = coreManager.save(Selling.class, s);
			}catch (Exception e) {
				SgwtRestErrorResponse resp = new SgwtRestErrorResponse(SgwtRestResponseBase.RESPONSE_FAILURE);
				resp.addError("exception", e.getMessage());
				return resp;
			}
			
		}else if(request.getOperationType() == OPERATION_TYPE.ADD){
									
			Double sellingPrice = RequestUtil.getDouble(request.getData().get("sellingPrice"));
			Integer measurementSelectionId = RequestUtil.getInteger(request.getData().get("measurementSelectionId"));
			
			Selling s = new Selling();
			s.setSellingPrice(sellingPrice);
			
			Measurement selm = coreManager.get(Measurement.class, measurementSelectionId);
			s.setMeasurement(selm);
			
			Product prod = coreManager.get(Product.class, id);
			s.setProduct(prod);
		
			try{
				respSelling = coreManager.save(Selling.class, s);
			}catch (Exception e) {
				e.printStackTrace();
				SgwtRestErrorResponse resp = new SgwtRestErrorResponse(-1);
				resp.addError("exception", e.getMessage());
				return resp;
			}
		}
		
		DisplaySelling displaySelling = null;
		
		if(respSelling != null){
			displaySelling = mapper.map(respSelling, DisplaySelling.class);
		}
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(displaySelling);
		
		return ret;
	}	
	
		
	
}
