package com.swinarta.sunflower.server.resources;

import java.util.List;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;

public class InfoResource extends ServerResource{

	private SessionRegistry sessionRegistry;
	
	public void setSessionRegistry(SessionRegistry sessionRegistry) {
		this.sessionRegistry = sessionRegistry;
	}

	@Get("json")
	public String represent(){
		
		StringBuffer str = new StringBuffer();
		str.append("Info: Seed is running \n");
		
		List<Object> principals = sessionRegistry.getAllPrincipals();
		if(principals.size() <= 0){
			str.append("no active session");
		}else{
			for (Object object : principals) {
				List<SessionInformation> sess = sessionRegistry.getAllSessions(object, false);
				if(sess.size() > 0){
					str.append(object.toString() + ":" + sess.get(0).getSessionId() + "\n");
				}
			}
		}
		
		return str.toString();
		
	}

}
