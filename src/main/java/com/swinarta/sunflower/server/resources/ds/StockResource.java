package com.swinarta.sunflower.server.resources.ds;

import org.dozer.Mapper;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.core.model.Stock;
import com.swinarta.sunflower.core.model.Stock.StockUpdateReason;
import com.swinarta.sunflower.server.model.DisplayStock;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.model.SgwtRequest.OPERATION_TYPE;
import com.swinarta.sunflower.server.util.RequestUtil;

public class StockResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}	

	@Post("json")
	public SgwtRestResponseBase getRepresent(SgwtRequest request){

		Integer productId = (Integer) request.getData().get("productId");

		if(request.getOperationType() == OPERATION_TYPE.FETCH){
			
			Integer storeId = (Integer) request.getData().get("storeId");
			
			Stock s = coreManager.getStock(productId, storeId);
			
			DisplayStock dstock = null;
			
			if(s != null){
				dstock = mapper.map(s, DisplayStock.class);
				dstock.setProductId(s.getProduct().getId());
			}
			
			SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(dstock);
					
			return ret;
		
		}else if(request.getOperationType() == OPERATION_TYPE.ADD){
			
			Float current = RequestUtil.getFloat(request.getData().get("current"));
			Integer max = (Integer) request.getData().get("max");
			Integer min = (Integer) request.getData().get("min");
			Integer order = (Integer) request.getData().get("defaultOrder");
			Integer storeId = (Integer) request.getData().get("storeId");
			
			Product product = coreManager.get(Product.class, productId);
			
			Stock s = new Stock();
			s.setCurrent(current);
			s.setDefaultOrder(order);
			s.setMax(max);
			s.setMin(min);
			s.setLastUpdateReason(StockUpdateReason.WEB);
			s.setProduct(product);
			s.setStoreId(storeId);
			
			Stock retStock;
			DisplayStock dstock = null;
			try {
				retStock = coreManager.save(Stock.class, s);
				dstock = mapper.map(retStock, DisplayStock.class);
				dstock.setProductId(retStock.getProduct().getId());
				
			} catch (Exception e) {
				SgwtRestErrorResponse resp = new SgwtRestErrorResponse(SgwtRestResponseBase.RESPONSE_FAILURE);
				resp.addError("exception", e.getMessage());
				return resp;
			}
			
			SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(dstock);
			
			return ret;
		}else if(request.getOperationType() == OPERATION_TYPE.UPDATE){

			Float current = RequestUtil.getFloat(request.getData().get("current"));
			Integer max = (Integer) request.getData().get("max");
			Integer min = (Integer) request.getData().get("min");
			Integer order = (Integer) request.getData().get("defaultOrder");
			Integer storeId = (Integer) request.getData().get("storeId");

			Stock s = coreManager.getStock(productId, storeId);
			s.setCurrent(current);
			s.setDefaultOrder(order);
			s.setMax(max);
			s.setMin(min);
			s.setLastUpdateReason(StockUpdateReason.WEB);
			
			Stock retStock;
			DisplayStock dstock = null;
			try {
				retStock = coreManager.save(Stock.class, s);
				dstock = mapper.map(retStock, DisplayStock.class);
				dstock.setProductId(retStock.getProduct().getId());				
			} catch (Exception e) {
				SgwtRestErrorResponse resp = new SgwtRestErrorResponse(SgwtRestResponseBase.RESPONSE_FAILURE);
				resp.addError("exception", e.getMessage());
				return resp;
			}
			
			SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(dstock);
			
			return ret;					
			
		}
		
		return null;
	}	
	
}