package com.swinarta.sunflower.server.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.server.model.DisplayProductWithStock;
import com.swinarta.sunflower.server.model.DisplayStock;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class StockSummaryResource extends ServerResource{

	protected CoreManager coreManager;

	private Mapper mapper;
	
	private Properties properties;
	
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		
		Integer storeId = Integer.parseInt(properties.getProperty("curr.store.id"));

		ResultList<DisplayProductWithStock> displayResultList = null;
		
		Integer supplierId = RequestUtil.getInteger(getQuery().getValues("supplierId"));
		Integer stockMode = RequestUtil.getInteger(getQuery().getValues("stockMode"));
		Integer start = RequestUtil.getInteger(getQuery().getValues("_startRow"));
		Integer end = RequestUtil.getInteger(getQuery().getValues("_endRow"));

		ResultList<Product> result = coreManager.searchProductWithStock(supplierId, null, null, null, false, storeId, stockMode, start, end);
		List<DisplayProductWithStock> displayProductList = new ArrayList<DisplayProductWithStock>();
		
		for (Product product : result) {
			DisplayProductWithStock disp = mapper.map(product, DisplayProductWithStock.class);
			if(product.getStock().size() > 0){
				disp.setStock(mapper.map(product.getStock().iterator().next(), DisplayStock.class));
			}
			displayProductList.add(disp);
		}
		
		displayResultList = new ResultList<DisplayProductWithStock>(displayProductList);
		displayResultList.setTotalCount(result.getTotalCount());
		displayResultList.setStartRow(result.getStartRow());
		displayResultList.setEndRow(result.getEndRow());			

		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(displayResultList);		
		return ret;
		
	}
}
