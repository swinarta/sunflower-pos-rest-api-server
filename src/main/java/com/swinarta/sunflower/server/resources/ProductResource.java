package com.swinarta.sunflower.server.resources;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.springframework.dao.DataIntegrityViolationException;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Category;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.server.model.DisplayProduct;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ProductResource extends ServerResource{
	
	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}	

	@Post("json")
	public SgwtRestResponseBase getRepresent(SgwtRequest request){
					
		Serializable resp = null;		
		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		String sku = (String) request.getData().get("sku");
		String barcode = (String) request.getData().get("barcode");
		String longDescription = (String) request.getData().get("longDescription");
		String shortDescription = (String) request.getData().get("shortDescription");
		Boolean consignment = (Boolean) request.getData().get("consignment");
		Boolean scallable = (Boolean) request.getData().get("scallable");
		Boolean deleteInd = !(Boolean) request.getData().get("active");
		Integer categoryId = RequestUtil.getInteger(request.getData().get("categoryId"));			
			
		Product p = coreManager.get(Product.class, id);
		p.setSku(sku);
		p.setBarcode(barcode);
		p.setLongDescription(longDescription);
		p.setShortDescription(shortDescription);
		p.setScallable(scallable);
		p.setConsignment(consignment);
		p.setDeleteInd(deleteInd);

		Category category = coreManager.getCategory(categoryId);
		p.setCategory(category);

		try {
			Product savedProduct = coreManager.save(Product.class, p);
			resp = mapper.map(savedProduct, DisplayProduct.class);
		}catch (DataIntegrityViolationException dive){
			
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-4);
			
			String contraintMsg = dive.getCause().getCause().getMessage();
			if(StringUtils.indexOf(contraintMsg, "sku") > 0){
				resp1.addError("sku", "SKU Found");
			}else if(StringUtils.indexOf(contraintMsg, "barcode") > 0){
				resp1.addError("barcode", "Barcode Found");
			}				
			return resp1;				
		} catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exeption", e.getMessage());
			return resp1;				
		}

		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;
		
	}	
	
}