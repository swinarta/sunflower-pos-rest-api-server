package com.swinarta.sunflower.server.resources;

import java.util.List;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Store;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;

public class StoresResource extends ServerResource{
	
	private CoreManager coreManager;

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		List<Store> stores = (List<Store>)coreManager.getAllStore();
		ResultList<Store> list = new ResultList<Store>(stores);
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(list);				
		return ret;
	}	
	
}