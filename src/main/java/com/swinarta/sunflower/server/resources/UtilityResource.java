package com.swinarta.sunflower.server.resources;

import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class UtilityResource extends ServerResource{

	private CoreManager coreManager;
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}	

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		String actionId = RequestUtil.getString(getQuery().getValues("actionId"));
		if(StringUtils.isNotEmpty(actionId) && actionId.equalsIgnoreCase("getNextSku")){
			String nextSku = coreManager.getNextSku();
			HashMap<String, String> resultMap = new HashMap<String, String>();
			resultMap.put("value", nextSku);
			SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resultMap);				
			return ret;
		}			
		
		return null;
	}
}
