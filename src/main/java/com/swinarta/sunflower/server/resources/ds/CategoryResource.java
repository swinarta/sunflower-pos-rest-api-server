package com.swinarta.sunflower.server.resources.ds;

import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.CategoryWithMainCategory;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.model.SgwtRequest.OPERATION_TYPE;

public class CategoryResource extends ServerResource{

	private CoreManager coreManager;
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}	

	@Post("json")
	public SgwtRestResponseBase getRepresent(SgwtRequest request){

		if(request.getOperationType() == OPERATION_TYPE.FETCH){			
		
			String actionId = (String) request.getData().get("actionId");
			
			if("getAll".equalsIgnoreCase(actionId)){
				ResultList<CategoryWithMainCategory> list = coreManager.getAllCategory();				
				SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(list);				
				return ret;
			}
			
		}else if(request.getOperationType() == OPERATION_TYPE.ADD){
			
		}else if(request.getOperationType() == OPERATION_TYPE.UPDATE){
			
		}
		
		return null;
	}	
	
}