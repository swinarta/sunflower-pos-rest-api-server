package com.swinarta.sunflower.server.resources;

import java.io.Serializable;

import org.dozer.Mapper;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.ReturnToSupplierDetail;
import com.swinarta.sunflower.server.model.DisplayProductMeasurement;
import com.swinarta.sunflower.server.model.DisplayReturnToSupplierDetail;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRequest.OPERATION_TYPE;
import com.swinarta.sunflower.server.model.SgwtRestDeleteResponse;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ReturnToSupplierDetailResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Post("json")
	public SgwtRestResponseBase action(SgwtRequest request){
		Serializable resp = null;

		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		
		if(request.getOperationType() == OPERATION_TYPE.UPDATE){
			Float qty = RequestUtil.getFloat(request.getData().get("qty"));
			
			ReturnToSupplierDetail ret = coreManager.get(ReturnToSupplierDetail.class, id);
			ret.setQty(qty);
			
			try {
				ReturnToSupplierDetail retResp = coreManager.save(ReturnToSupplierDetail.class, ret);
				retResp = coreManager.getReturnToSupplierDetail(retResp.getId());
				DisplayReturnToSupplierDetail det =  mapper.map(retResp, DisplayReturnToSupplierDetail.class);
				det.setCostPrice(retResp.getProduct().getBuying().getCostPrice());

				ProductMeasurement productMeasurement = null;
				if(retResp.getProduct().getProductMeasurement() != null && !retResp.getProduct().getProductMeasurement().isEmpty()){
					productMeasurement = retResp.getProduct().getProductMeasurement().iterator().next();
				}
				
				if(productMeasurement != null){
					det.getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
				}			
				
				resp = det;
				
			} catch (Exception e) {
				SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
				resp1.addError("exception", e.getMessage());
				return resp1;				
			}		
			
		}else if(request.getOperationType() == OPERATION_TYPE.REMOVE){
			ReturnToSupplierDetail retd = coreManager.get(ReturnToSupplierDetail.class, id);
			coreManager.remove(retd);
			return new SgwtRestDeleteResponse(id);
		}		
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		
		return ret;
		

	}
}
