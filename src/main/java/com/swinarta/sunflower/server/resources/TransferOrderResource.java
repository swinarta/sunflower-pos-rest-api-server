package com.swinarta.sunflower.server.resources;

import java.io.Serializable;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.TransferOrder;
import com.swinarta.sunflower.core.model.TransferOrder.Status;
import com.swinarta.sunflower.server.model.DisplayTransferOrder;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class TransferOrderResource extends ServerResource{

	private CoreManager coreManager;
	
	private Mapper mapper;

	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}
	
	@Get("json")
	public SgwtRestResponseBase fetch(){
		Serializable resp = null;		
		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		TransferOrder transfer = coreManager.get(TransferOrder.class, id);
		if(transfer != null){
			DisplayTransferOrder dto = mapper.map(transfer, DisplayTransferOrder.class);
			resp = dto;
		}
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;
		
	}
	
	@Post("json")
	public SgwtRestResponseBase update(SgwtRequest req){
		Serializable resp = null;		
		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		String remarks = RequestUtil.getString(req.getData().get("remarks"));
		String status = RequestUtil.getString(req.getData().get("status"));
		
		Status TOStatus = Status.fromString(status);
		TransferOrder transferResp = null;
		TransferOrder to = coreManager.get(TransferOrder.class, id);
		
		Status oldStatus = to.getStatus();
		Boolean isProcessed = (!oldStatus.equals(TOStatus) && TOStatus == Status.PROCESSED);
		Boolean isCancelledFromProcessed = (oldStatus == Status.PROCESSED && TOStatus == Status.CANCELLED);
		Boolean isCancelledFromNew = (oldStatus == Status.NEW && TOStatus == Status.CANCELLED);
		Boolean isCompleted = (!oldStatus.equals(TOStatus) && TOStatus == Status.COMPLETED);
		
		try {
			if(isProcessed){
				transferResp = coreManager.updateProcessTransferOrder(id);
			}else if(isCancelledFromNew){
				transferResp = coreManager.updateCancelNewTransferOrder(id);
			}else if(isCancelledFromProcessed){
				transferResp = coreManager.updateCancelProcessTransferOrder(id);
			}else if(isCompleted){
				transferResp = coreManager.updateCompletedTransferOrder(id);
			}else{
				to.setRemarks(remarks);
				to.setStatus(TOStatus);
				transferResp = coreManager.save(TransferOrder.class, to);
			}
			DisplayTransferOrder dto = mapper.map(transferResp, DisplayTransferOrder.class);
			resp = dto;
		} catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;
	}
}
