package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Store;
import com.swinarta.sunflower.core.model.TransferOrder;
import com.swinarta.sunflower.core.model.TransferOrder.Status;
import com.swinarta.sunflower.server.model.DisplayTransferOrder;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;
import com.swinarta.sunflower.server.util.TransferUtil;

public class TransferOrdersResource extends ServerResource{

	private CoreManager coreManager;
	
	private Mapper mapper;
		
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}
	
	@Get("json")
	public SgwtRestFetchResponseBase getRepresent(){
		SgwtRestFetchResponseBase resp = null;
		String statusStr = RequestUtil.getString(getQuery().getValues("toStatusStr"));
		Integer start = RequestUtil.getInteger(getQuery().getValues("_startRow"));
		Integer end = RequestUtil.getInteger(getQuery().getValues("_endRow"));
		
		Status status = Status.fromString(statusStr);

		ResultList<TransferOrder> list = coreManager.searchTransferOrder(status, start, end);
		List<DisplayTransferOrder> displayList = new ArrayList<DisplayTransferOrder>();
		
		for (TransferOrder transferOrder : list) {
			displayList.add(mapper.map(transferOrder, DisplayTransferOrder.class));
		}
		
		
		ResultList<DisplayTransferOrder> resultList = new ResultList<DisplayTransferOrder>(displayList);
						
		resp = new SgwtRestFetchResponseBase(resultList);
		return resp;
	}
	
	@Post("json")
	public SgwtRestResponseBase add(SgwtRequest req){
		Serializable resp = null;
		String remarks = RequestUtil.getString(req.getData().get("remarks"));
		String fromStoreCode = RequestUtil.getString(req.getData().get("fromStoreCode"));
		String toStoreCode = RequestUtil.getString(req.getData().get("toStoreCode"));
		
		Store fromStore = coreManager.getStore(fromStoreCode);
		Store toStore = coreManager.getStore(toStoreCode);
				
		TransferOrder transfer = new TransferOrder();
		transfer.setTransferDate(new Date());
		transfer.setFromStoreCode(fromStoreCode);
		transfer.setToStoreCode(toStoreCode);
		transfer.setTransferId("temp");
		transfer.setRemarks(remarks);
		transfer.setStatus(com.swinarta.sunflower.core.model.TransferOrder.Status.NEW);
		
		
		try {
			TransferOrder transferResp = coreManager.save(TransferOrder.class, transfer);
			transferResp.setTransferId(TransferUtil.constructCode(transfer.getTransferDate(), fromStore.getId(), toStore.getId(), transferResp.getId()));
			transferResp = coreManager.save(TransferOrder.class, transferResp);
			
			DisplayTransferOrder dto = mapper.map(transferResp, DisplayTransferOrder.class);
			resp = dto;
			
		} catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;
	}
}
