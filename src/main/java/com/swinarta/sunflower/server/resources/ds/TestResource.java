package com.swinarta.sunflower.server.resources.ds;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Buying;
import com.swinarta.sunflower.server.model.DisplayBuying;

public class TestResource extends ServerResource{

	private CoreManager coreManager;
	
	private Mapper mapper;
		
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}	

	@Get("json")
	public Object getMe(){
		Buying b = coreManager.get(Buying.class, 1);

		return mapper.map(b, DisplayBuying.class);
				
	}
}
