package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.Date;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.PurchasingOrder;
import com.swinarta.sunflower.core.model.PurchasingOrder.Status;
import com.swinarta.sunflower.server.model.DisplaySearchPurchasingOrder;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class PurchasingOrderResource extends ServerResource{

	private CoreManager coreManager;
	
	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}
	
	@Get("json")
	public SgwtRestResponseBase fetch(){
		Serializable resp = null;		
		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		PurchasingOrder po = coreManager.get(PurchasingOrder.class, id);
		if(po != null){
			DisplaySearchPurchasingOrder dpo = mapper.map(po, DisplaySearchPurchasingOrder.class);
			resp = dpo;
		}
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;
		
	}
	
	@Post("json")
	public SgwtRestResponseBase update(SgwtRequest req){
		Serializable resp = null;		
		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		Date poDate = RequestUtil.getDate(req.getData().get("poDate")); 
		Date cancelDate = RequestUtil.getDate(req.getData().get("cancelDate"));
		String remarks = RequestUtil.getString(req.getData().get("remarks"));
		String status = RequestUtil.getString(req.getData().get("status"));
		
		Status POStatus = Status.fromString(status);
		
		try {
			PurchasingOrder poResp = null;
			if(POStatus.equals(Status.CANCELLED)){
				poResp = coreManager.updateCancelPurchasingOrder(id);
			}else{
				PurchasingOrder po = coreManager.get(PurchasingOrder.class, id);
				po.setPoDate(poDate);
				po.setCancelDate(cancelDate);
				po.setRemarks(remarks);
				po.setStatus(POStatus);
				poResp = coreManager.save(PurchasingOrder.class, po);
			}
			DisplaySearchPurchasingOrder dpo = mapper.map(poResp, DisplaySearchPurchasingOrder.class);
			resp = dpo;
		} catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;
	}
}
