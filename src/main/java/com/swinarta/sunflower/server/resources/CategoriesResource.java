package com.swinarta.sunflower.server.resources;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.CategoryWithMainCategory;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;

public class CategoriesResource extends ServerResource{

	private CoreManager coreManager;
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestFetchResponseBase getRepresent(){
		ResultList<CategoryWithMainCategory> list = coreManager.getAllCategory();				
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(list);				
		return ret;
	}
}
