package com.swinarta.sunflower.server.resources;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Measurement;

public class MeasurementResource extends ServerResource{

	protected CoreManager coreManager;

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}			

	@Get("json")
	public Measurement represent(){
		
		String idStr = (String) getRequest().getAttributes().get("id");
		Integer id = Integer.parseInt(idStr);			
		return coreManager.get(Measurement.class, id);
		
	}
	
}
