package com.swinarta.sunflower.server.resources;

import org.apache.commons.lang.StringUtils;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.springframework.dao.DataIntegrityViolationException;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Supplier;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class SuppliersResource extends ServerResource{

	private CoreManager coreManager;
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		
		String text = RequestUtil.getString(getQuery().getValues("searchSupplier"));
		Integer start = RequestUtil.getInteger(getQuery().getValues("_startRow"));
		Integer end = RequestUtil.getInteger(getQuery().getValues("_endRow"));
		
		ResultList<Supplier> list;
		
		if(StringUtils.isNotEmpty(text)){					
			list = coreManager.searchSupplier(text, start, end);
		}else{
			list = coreManager.getAllSupplier();
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(list);
		return ret;
	}
	
	@Post("json")
	public SgwtRestResponseBase post(SgwtRequest request){
		String supplierCode = RequestUtil.getString(request.getData().get("supplierCode"));
		String supplierName = RequestUtil.getString(request.getData().get("name"));
		String contactName = RequestUtil.getString(request.getData().get("contactName"));
		String addr1 = RequestUtil.getString(request.getData().get("address1"));
		String addr2 = RequestUtil.getString(request.getData().get("address2"));
		String city = RequestUtil.getString(request.getData().get("city"));
		String phone = RequestUtil.getString(request.getData().get("phone"));
		String fax = RequestUtil.getString(request.getData().get("fax"));
		String mobile = RequestUtil.getString(request.getData().get("mobile"));
		Integer termOfPayment = RequestUtil.getInteger(request.getData().get("termOfPayment"));

		Supplier s = new Supplier();
		s.setSupplierCode(supplierCode);
		s.setName(supplierName);
		s.setContactName(contactName);
		s.setAddress1(addr1);
		s.setAddress2(addr2);
		s.setCity(city);
		s.setPhone(phone);
		s.setFax(fax);
		s.setMobile(mobile);
		s.setTermOfPayment(termOfPayment);
		
		Supplier savedSupp;
		
		try {
				savedSupp = coreManager.save(Supplier.class, s);
		}catch (DataIntegrityViolationException dive){
			SgwtRestErrorResponse resp = new SgwtRestErrorResponse(-4);
			
			String contraintMsg = dive.getCause().getCause().getMessage();
			if(StringUtils.indexOf(contraintMsg, "key 2") > 0){
				resp.addError("supplierCode", "Supplier Code Found");
			}				
			return resp;				

		}catch (Exception e) {
			e.printStackTrace();
			SgwtRestErrorResponse resp = new SgwtRestErrorResponse(-1);
			resp.addError("exeption", e.getMessage());
			return resp;
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(savedSupp);
		
		return ret;
		}
	
}
