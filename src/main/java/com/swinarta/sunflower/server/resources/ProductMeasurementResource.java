package com.swinarta.sunflower.server.resources;

import java.io.Serializable;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.server.model.DisplayProductMeasurement;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ProductMeasurementResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}
	
	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		Integer pid = RequestUtil.getInteger(getRequestAttributes().get("productId"));
		Integer mid = RequestUtil.getInteger(getRequestAttributes().get("measurementId"));
		Serializable resp = null;
		
		ProductMeasurement pm = coreManager.getProductMeasurement(pid, mid);
		
		DisplayProductMeasurement dpm = null;
		if(pm != null){
			dpm = mapper.map(pm, DisplayProductMeasurement.class);		
		}
		resp = dpm;
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);		
		return ret;
		
	}

}
