package com.swinarta.sunflower.server.resources;

import java.io.Serializable;

import org.dozer.Mapper;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.PurchasingOrderDetail;
import com.swinarta.sunflower.core.model.Stock;
import com.swinarta.sunflower.server.model.DisplayProductMeasurement;
import com.swinarta.sunflower.server.model.DisplayPurchasingOrderDetail;
import com.swinarta.sunflower.server.model.DisplayStock;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestDeleteResponse;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.model.SgwtRequest.OPERATION_TYPE;
import com.swinarta.sunflower.server.util.RequestUtil;

public class PurchasingOrderDetailResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Post("json")
	public SgwtRestResponseBase action(SgwtRequest request){
		Serializable resp = null;

		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		
		if(request.getOperationType() == OPERATION_TYPE.UPDATE){
			Float qty = RequestUtil.getFloat(request.getData().get("qty"));
			
			PurchasingOrderDetail pod = coreManager.get(PurchasingOrderDetail.class, id);
			pod.setQty(qty);
			
			try {
				PurchasingOrderDetail podResp = coreManager.save(PurchasingOrderDetail.class, pod);
				podResp = coreManager.getPurchasingOrderDetail(pod.getId());
				DisplayPurchasingOrderDetail det =  mapper.map(podResp, DisplayPurchasingOrderDetail.class);
				det.setCostPrice(podResp.getProduct().getBuying().getCostPrice());

				Stock stock = null;
				ProductMeasurement productMeasurement = null;			
				if(podResp.getProduct().getStock() != null && !podResp.getProduct().getStock().isEmpty()){
					stock = podResp.getProduct().getStock().iterator().next();
				}
				if(podResp.getProduct().getProductMeasurement() != null && !podResp.getProduct().getProductMeasurement().isEmpty()){
					productMeasurement = podResp.getProduct().getProductMeasurement().iterator().next();
				}
				if(stock != null){
					det.getProduct().setStock(mapper.map(stock, DisplayStock.class));
				}
				
				if(productMeasurement != null){
					det.getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
				}			
				
				resp = det;
				
			} catch (Exception e) {
				SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
				resp1.addError("exception", e.getMessage());
				return resp1;				
			}		
			
		}else if(request.getOperationType() == OPERATION_TYPE.REMOVE){
			PurchasingOrderDetail pod = coreManager.get(PurchasingOrderDetail.class, id);
			coreManager.remove(pod);
			return new SgwtRestDeleteResponse(id);
		}		
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		
		return ret;
		

	}
}
