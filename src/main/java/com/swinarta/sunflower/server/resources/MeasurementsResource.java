package com.swinarta.sunflower.server.resources;

import java.util.Collection;

import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Measurement;

public class MeasurementsResource extends ServerResource{

	protected CoreManager coreManager;

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}			

	@Get("json")
	public Collection<Measurement> represent(){
		return coreManager.getAllMeasurement();
	}	
	
	@Post("json")
	public Measurement post(Measurement m) throws Exception{    
    	return coreManager.save(Measurement.class, m);
	}
		
}
