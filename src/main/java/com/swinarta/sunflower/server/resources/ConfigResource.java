package com.swinarta.sunflower.server.resources;

import java.util.Properties;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;

public class ConfigResource extends ServerResource{

	private Properties properties;

	
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){		
		return new SgwtRestFetchResponseBase(properties);		
	}
	
}