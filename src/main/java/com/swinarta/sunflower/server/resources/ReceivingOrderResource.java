package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.Date;

import org.dozer.Mapper;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ReceivingOrder;
import com.swinarta.sunflower.core.model.ReceivingOrder.Status;
import com.swinarta.sunflower.server.model.DisplayReceivingOrder;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ReceivingOrderResource extends ServerResource{

	private CoreManager coreManager;
	
	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}	
	
	@Post("json")
	public SgwtRestResponseBase update(SgwtRequest req){
		Serializable resp = null;
		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		Date roDate = RequestUtil.getDate(req.getData().get("roDate")); 
		String remarks = RequestUtil.getString(req.getData().get("remarks"));
		String status = RequestUtil.getString(req.getData().get("status"));
		Boolean isCompleted = false;
		
		ReceivingOrder ro = coreManager.get(ReceivingOrder.class, id);		
				
		if(status != null){
			Status oldStatus = ro.getStatus();
			Status newStatus = Status.fromString(status);
			isCompleted = (!oldStatus.equals(newStatus) && newStatus == Status.COMPLETED);
		}
		
		try {
			ReceivingOrder roResp;
			if(isCompleted){
				roResp = coreManager.updateCompleteReceivingOrder(ro.getId());
			}else{
				ro.setRoDate(roDate);
				ro.setRemarks(remarks);				
				roResp = coreManager.save(ReceivingOrder.class, ro);
			}
			DisplayReceivingOrder dro = mapper.map(roResp, DisplayReceivingOrder.class);
			resp = dro;
		} catch (Exception e) {
			e.printStackTrace();
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;
	}
}
