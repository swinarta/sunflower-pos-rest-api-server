package com.swinarta.sunflower.server.resources;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.ReceivingOrderDetail;
import com.swinarta.sunflower.server.model.DisplayProductMeasurement;
import com.swinarta.sunflower.server.model.DisplayReceivingOrderDetail;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ReceivingOrderDetailsResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		SgwtRestFetchResponseBase resp = null;
		Integer roId = RequestUtil.getInteger(getRequestAttributes().get("roId"));
		
		List<ReceivingOrderDetail> details = coreManager.getReceivingOrderDetails(roId);
		
		List<DisplayReceivingOrderDetail> resultList = new ArrayList<DisplayReceivingOrderDetail>();
		
		for (ReceivingOrderDetail receivingOrderDetail : details) {
			
			ProductMeasurement productMeasurement = null;
			
			if(receivingOrderDetail.getPoDetail().getProduct().getProductMeasurement() != null && !receivingOrderDetail.getPoDetail().getProduct().getProductMeasurement().isEmpty()){
				productMeasurement = receivingOrderDetail.getPoDetail().getProduct().getProductMeasurement().iterator().next();
			}
						
			DisplayReceivingOrderDetail det = mapper.map(receivingOrderDetail, DisplayReceivingOrderDetail.class);
			det.setCostPrice(receivingOrderDetail.getPoDetail().getProduct().getBuying().getCostPrice());
			
			if(productMeasurement != null){
				det.getPoDetail().getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
			}			
			
			resultList.add(det);
		}
		
		resp = new SgwtRestFetchResponseBase(resultList);
		
		return resp;

	}
	
}
