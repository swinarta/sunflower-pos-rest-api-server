package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.Date;

import org.dozer.Mapper;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.PurchasingOrder;
import com.swinarta.sunflower.core.model.ReceivingOrder;
import com.swinarta.sunflower.server.model.DisplayReceivingOrder;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;
import com.swinarta.sunflower.server.util.RoUtil;

public class ReceivingOrdersResource extends ServerResource{

	private CoreManager coreManager;
	
	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Post("json")
	public SgwtRestResponseBase add(SgwtRequest req){
		Serializable resp = null;
		Integer poId = RequestUtil.getInteger(req.getData().get("poId"));
		
		Date today = new Date();

		try {
			PurchasingOrder po = coreManager.get(PurchasingOrder.class, poId);		
			ReceivingOrder ro = coreManager.createReceivingOrder(poId, today);
			
			String roId = RoUtil.constructRoId(today, po.getSupplier().getId(), ro.getId());
			ro.setRoId(roId);
			ro = coreManager.save(ReceivingOrder.class, ro);

			DisplayReceivingOrder disp = mapper.map(ro, DisplayReceivingOrder.class);
			resp = disp;
		} catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;

	}
}
