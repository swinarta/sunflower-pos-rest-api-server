package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.springframework.dao.DataIntegrityViolationException;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Category;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.server.model.DisplayProduct;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ProductsResource extends ServerResource{

	protected CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){

		ResultList<DisplayProduct> displayResultList = null;
			
		String text = RequestUtil.getString(getQuery().getValues("searchProduct"));
		String actionId = RequestUtil.getString(getQuery().getValues("actionId"));
		Integer supplierId = RequestUtil.getInteger(getQuery().getValues("supplierId"));
		Integer start = RequestUtil.getInteger(getQuery().getValues("_startRow"));
		Integer end = RequestUtil.getInteger(getQuery().getValues("_endRow"));
				
		if(actionId == null || (StringUtils.isNotEmpty(actionId) && actionId.equalsIgnoreCase("searchProduct"))){
			Boolean deleteInd = RequestUtil.getBoolean(getQuery().getValues("deleteInd"));
			Boolean hasSelling = RequestUtil.getBoolean(getQuery().getValues("hasSelling"));

			String barcode = null;
			String sku = null;
			String description = null;
			
			if(StringUtils.isNumeric(text)){
				
				if(text.length() > 10){
					barcode = text;
				}else{
					sku = text;
				}
				
			}else{
				description = text;
			}

			ResultList<Product> result = coreManager.searchProduct(supplierId, barcode, sku, description, deleteInd, hasSelling, start, end);
			List<DisplayProduct> displayProductList = new ArrayList<DisplayProduct>();
			
			for (Product product : result) {
				displayProductList.add(mapper.map(product, DisplayProduct.class));
			}
			
			displayResultList = new ResultList<DisplayProduct>(displayProductList);
			displayResultList.setTotalCount(result.getTotalCount());
			displayResultList.setStartRow(result.getStartRow());
			displayResultList.setEndRow(result.getEndRow());			

		}else if(StringUtils.isNotEmpty(actionId) && actionId.equalsIgnoreCase("searchRelated")){
			String description = RequestUtil.getString(getQuery().getValues("description"));
			String descriptionToSearch = StringUtils.substringBefore(description, " ");
			
			Integer categoryId = RequestUtil.getInteger(getQuery().getValues("categoryId"));
			Integer productId = RequestUtil.getInteger(getQuery().getValues("productId"));
			
			ResultList<Product> productResult = coreManager.searchRelatedProduct(productId, categoryId, descriptionToSearch, start, end);
			
			List<DisplayProduct> displayProductList = new ArrayList<DisplayProduct>();
			
			for (Product product : productResult) {
				displayProductList.add(mapper.map(product, DisplayProduct.class));
			}
			
			displayResultList = new ResultList<DisplayProduct>(displayProductList);
			displayResultList.setTotalCount(productResult.getTotalCount());
			displayResultList.setStartRow(productResult.getStartRow());
			displayResultList.setEndRow(productResult.getEndRow());
			
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(displayResultList);		
		return ret;
				
	}
	
	@Post("json")
	public SgwtRestResponseBase add(SgwtRequest request){

		Serializable resp = null;
		
		String sku = RequestUtil.getString(request.getData().get("sku"));
		String barcode = RequestUtil.getString(request.getData().get("barcode"));
		String longDescription = RequestUtil.getString(request.getData().get("longDescription"));
		String shortDescription = RequestUtil.getString(request.getData().get("shortDescription"));
		Boolean consignment = RequestUtil.getBoolean(request.getData().get("consignment"));
		Boolean scallable = RequestUtil.getBoolean(request.getData().get("scallable"));
		Integer categoryId = RequestUtil.getInteger(request.getData().get("categoryId"));			

		Product p = new Product();
		p.setSku(sku);
		p.setBarcode(barcode);
		p.setLongDescription(longDescription);
		p.setShortDescription(shortDescription);
		p.setScallable(scallable);
		p.setConsignment(consignment);
		p.setDeleteInd(false);

		Category category = coreManager.getCategory(categoryId);
		p.setCategory(category);
		
		try {
			Product savedProduct = coreManager.save(Product.class, p);
						
			resp = mapper.map(savedProduct, DisplayProduct.class);
		}catch (DataIntegrityViolationException dive){
			
			dive.printStackTrace();
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-4);
			
			try{
				String contraintMsg = dive.getCause().getCause().getMessage();
				if(StringUtils.indexOf(contraintMsg, "key 'sku'") > 0){
					resp1.addError("sku", "SKU Found");
				}else if(StringUtils.indexOf(contraintMsg, "key 'barcode'") > 0){
					resp1.addError("barcode", "Barcode Found");
				}				
				return resp1;				
			}catch (Exception e) {
				e.printStackTrace();
				SgwtRestErrorResponse resp2 = new SgwtRestErrorResponse(-1);
				resp2.addError("exeption", e.getMessage());
				return resp2;				
			}
		} catch (Exception e) {
			e.printStackTrace();
			SgwtRestErrorResponse resp3 = new SgwtRestErrorResponse(-1);
			resp3.addError("exeption", e.getMessage());
			return resp3;
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;

	}
}
