package com.swinarta.sunflower.server.resources;

import java.io.Serializable;

import org.dozer.Mapper;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.TransferOrderDetail;
import com.swinarta.sunflower.server.model.DisplayProductMeasurement;
import com.swinarta.sunflower.server.model.DisplayTransferOrderDetail;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestDeleteResponse;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.model.SgwtRequest.OPERATION_TYPE;
import com.swinarta.sunflower.server.util.RequestUtil;

public class TransferOrderDetailResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Post("json")
	public SgwtRestResponseBase action(SgwtRequest request){
		Serializable resp = null;

		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		
		if(request.getOperationType() == OPERATION_TYPE.UPDATE){
			Float qty = RequestUtil.getFloat(request.getData().get("qty"));
			
			TransferOrderDetail pod = coreManager.get(TransferOrderDetail.class, id);
			pod.setQty(qty);
			
			try {
				TransferOrderDetail podResp = coreManager.save(TransferOrderDetail.class, pod);
				podResp = coreManager.getTransferOrderDetail(pod.getId());
				DisplayTransferOrderDetail det =  mapper.map(podResp, DisplayTransferOrderDetail.class);

				ProductMeasurement productMeasurement = null;			
				if(podResp.getProduct().getProductMeasurement() != null && !podResp.getProduct().getProductMeasurement().isEmpty()){
					productMeasurement = podResp.getProduct().getProductMeasurement().iterator().next();
				}
				
				if(productMeasurement != null){
					det.getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
				}			
				
				resp = det;
				
			} catch (Exception e) {
				SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
				resp1.addError("exception", e.getMessage());
				return resp1;				
			}		
			
		}else if(request.getOperationType() == OPERATION_TYPE.REMOVE){
			TransferOrderDetail pod = coreManager.get(TransferOrderDetail.class, id);
			coreManager.remove(pod);
			return new SgwtRestDeleteResponse(id);
		}		
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		
		return ret;
		

	}
}
