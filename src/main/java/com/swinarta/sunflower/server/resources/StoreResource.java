package com.swinarta.sunflower.server.resources;

import java.util.Properties;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Store;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;

public class StoreResource extends ServerResource{
	
	private CoreManager coreManager;

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	private Properties properties;

	
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		
		Integer storeId = Integer.parseInt(properties.getProperty("curr.store.id"));
				
		Store s = coreManager.get(Store.class, storeId);
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(s);
				
		return ret;
		
	}	
	
}