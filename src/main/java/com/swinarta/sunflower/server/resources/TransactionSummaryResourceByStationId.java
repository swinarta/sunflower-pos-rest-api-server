package com.swinarta.sunflower.server.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.server.model.DisplayTransactionSummary;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class TransactionSummaryResourceByStationId extends ServerResource{

	private CoreManager coreManager;
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase represent(){		

		Date date = RequestUtil.getDate(getRequestAttributes().get("date"));

		List<Object> summObject =  coreManager.findTransactionSummaryByStation(date);
		List<DisplayTransactionSummary> summ = new ArrayList<DisplayTransactionSummary>(); 
		
		for (Object object : summObject) {
			if(object instanceof Object[]){
				Object[] objs = (Object[])object;
				DisplayTransactionSummary dts = new DisplayTransactionSummary();
				dts.setStationId((Integer) objs[1]);
				dts.setTotal((Long) objs[0]);
				dts.setCount((Long) objs[2]);
				summ.add(dts);
			}
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(summ);
		return ret;		
	}

}
