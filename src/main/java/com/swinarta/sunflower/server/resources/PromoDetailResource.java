package com.swinarta.sunflower.server.resources;

import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.PromoDetail;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestDeleteResponse;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class PromoDetailResource extends ServerResource{

	private CoreManager coreManager;
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Post("json")
	public SgwtRestResponseBase add(SgwtRequest request){

		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		PromoDetail detail = coreManager.get(PromoDetail.class, id);
		coreManager.remove(detail);
		
		return new SgwtRestDeleteResponse(id);
		
	}
		
}
