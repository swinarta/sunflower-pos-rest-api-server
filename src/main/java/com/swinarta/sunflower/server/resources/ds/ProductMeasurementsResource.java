package com.swinarta.sunflower.server.resources.ds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Measurement;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.ProductMeasurementBase;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRequest.OPERATION_TYPE;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ProductMeasurementsResource extends ServerResource{

	private CoreManager coreManager;
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}	

	@Post("json")
	public SgwtRestFetchResponseBase getRepresent(SgwtRequest request) throws Exception{

		ResultList<ProductMeasurementBase> resultList = new ResultList<ProductMeasurementBase>(new ArrayList<ProductMeasurementBase>());
		Integer productId = RequestUtil.getInteger(request.getData().get("productId"));

		if(request.getOperationType() == OPERATION_TYPE.FETCH){
			
			Map<Integer, ProductMeasurement> productMeasurementMap = new HashMap<Integer, ProductMeasurement>();
			
			Collection<Measurement> measurementList = coreManager.getAllMeasurement();
			ResultList<ProductMeasurement> productMeasurementList = coreManager.searchProductMeasurement(productId);
			for (ProductMeasurement productMeasurement : productMeasurementList) {
				productMeasurementMap.put(productMeasurement.getMeasurement().getId(), productMeasurement);
			}
			
			for (Measurement measurement : measurementList) {
				ProductMeasurementBase pmb = new ProductMeasurementBase();
				pmb.setId(measurement.getId());
				pmb.setDescription(measurement.getDescription());
				pmb.setIsMutable(measurement.getMutable());

				if(productMeasurementMap.get(measurement.getId()) != null){
					ProductMeasurement pm = productMeasurementMap.get(measurement.getId());
					pmb.setQty(pm.getOverrideQty());
					pmb.setIsDefault(false);
				}else{
					pmb.setQty(measurement.getDefaultQty());
					pmb.setIsDefault(true);
				}
				
				resultList.add(pmb);
			}
			
		}else if(request.getOperationType() == OPERATION_TYPE.ADD){
			
			Map<Integer, ProductMeasurement> productMeasurementMap = new HashMap<Integer, ProductMeasurement>();
			ResultList<ProductMeasurement> productMeasurementList = coreManager.searchProductMeasurement(productId);
			for (ProductMeasurement productMeasurement : productMeasurementList) {
				productMeasurementMap.put(productMeasurement.getMeasurement().getId(), productMeasurement);
			}
			
			Collection<Measurement> measurementList = coreManager.getAllMeasurement();			
			for (Measurement measurement : measurementList) {
				
				Integer id = measurement.getId();
				if(measurement.getMutable()){
					Integer overrideQty = RequestUtil.getInteger(request.getData().get(String.valueOf(id)));
					
					ProductMeasurement pm = productMeasurementMap.get(id);
					
					if(overrideQty.intValue() == measurement.getDefaultQty().intValue()){
						//default qty, delete override measurement
						if(pm != null){							
							coreManager.remove(pm);
							productMeasurementMap.remove(id);
						}
					}else{
						if(pm != null){
							pm.setOverrideQty(overrideQty);
							ProductMeasurement pm1 = coreManager.save(ProductMeasurement.class, pm);	
							productMeasurementMap.put(id, pm1);
						}else{
							pm = new ProductMeasurement();
							Product p = coreManager.get(Product.class, productId);
							pm.setProduct(p);
							pm.setOverrideQty(overrideQty);
							pm.setMeasurement(measurement);
							
							ProductMeasurement pm1 = coreManager.save(ProductMeasurement.class, pm);	
							productMeasurementMap.put(id, pm1);							
						}
					}

				}

				ProductMeasurementBase pmb = new ProductMeasurementBase();
				pmb.setId(id);
				pmb.setDescription(measurement.getDescription());
				pmb.setIsMutable(measurement.getMutable());

				if(productMeasurementMap.get(id) != null){
					ProductMeasurement pm = productMeasurementMap.get(id);
					pmb.setQty(pm.getOverrideQty());
					pmb.setIsDefault(false);
				}else{
					pmb.setQty(measurement.getDefaultQty());
					pmb.setIsDefault(true);
				}
				
				resultList.add(pmb);

			}
			
		}

		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resultList);
		return ret;

	}

}