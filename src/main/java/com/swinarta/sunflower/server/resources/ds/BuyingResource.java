package com.swinarta.sunflower.server.resources.ds;

import org.dozer.Mapper;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.springframework.orm.ObjectRetrievalFailureException;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Buying;
import com.swinarta.sunflower.core.model.Measurement;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.core.model.Supplier;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRequest.OPERATION_TYPE;
import com.swinarta.sunflower.server.model.DisplayBuying;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class BuyingResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Post("json")
	public SgwtRestResponseBase getRepresent(SgwtRequest request){
		
		Buying respBuying = null;
		Integer id = (Integer) request.getData().get("id");
		
		if(request.getOperationType() == OPERATION_TYPE.FETCH){
			
			try{
				respBuying = coreManager.get(Buying.class, id);
			}catch (ObjectRetrievalFailureException e) {
				//buying info not found
			}
			
		}else if(request.getOperationType() == OPERATION_TYPE.UPDATE){

			Double buyingPrice = ((Number) request.getData().get("buyingPrice")).doubleValue();
			Double disc1 = ((Number) request.getData().get("disc1")).doubleValue();
			Double disc2 = ((Number) request.getData().get("disc2")).doubleValue();
			Double disc3 = ((Number) request.getData().get("disc3")).doubleValue();
			Double disc4 = ((Number) request.getData().get("disc4")).doubleValue();
			Boolean taxIncluded = (Boolean) request.getData().get("taxIncluded");
			Double discPrice = ((Number) request.getData().get("discPrice")).doubleValue();
			
			Integer measurementSelectionId = RequestUtil.getInteger(request.getData().get("measurementSelectionId"));
			Integer supplierId = RequestUtil.getInteger(request.getData().get("supplierId"));
			
			Buying b = coreManager.get(Buying.class, id);
			b.setBuyingPrice(buyingPrice);
			b.setDisc1(disc1);
			b.setDisc2(disc2);
			b.setDisc3(disc3);
			b.setDisc4(disc4);
			b.setTaxIncluded(taxIncluded);
			b.setDiscPrice(discPrice);

			if(measurementSelectionId.intValue() != b.getMeasurement().getId().intValue()){
				Measurement selm = coreManager.get(Measurement.class, measurementSelectionId);
				b.setMeasurement(selm);
			}
			
			if(supplierId.intValue() != b.getSupplier().getId().intValue()){
				Supplier supp = coreManager.get(Supplier.class, supplierId);
				b.setSupplier(supp);
			}
						
			try{
				respBuying = coreManager.save(Buying.class, b);
			}catch (Exception e) {
				SgwtRestErrorResponse resp = new SgwtRestErrorResponse(SgwtRestResponseBase.RESPONSE_FAILURE);
				resp.addError("exception", e.getMessage());
				return resp;
			}
			
		}else if(request.getOperationType() == OPERATION_TYPE.ADD){
			
			Double buyingPrice = RequestUtil.getDouble(request.getData().get("buyingPrice"));
			Double disc1 = RequestUtil.getDouble(request.getData().get("disc1"));
			Double disc2 = RequestUtil.getDouble(request.getData().get("disc2"));
			Double disc3 = RequestUtil.getDouble(request.getData().get("disc3"));
			Double disc4 = RequestUtil.getDouble(request.getData().get("disc4"));			
			Boolean taxIncluded = RequestUtil.getBoolean(request.getData().get("taxIncluded"));			
			Double discPrice = RequestUtil.getDouble(request.getData().get("discPrice"));
			
			Integer measurementSelectionId = RequestUtil.getInteger(request.getData().get("measurementSelectionId"));
			Integer supplierId = RequestUtil.getInteger(request.getData().get("supplierId"));

			Buying b = new Buying();
			b.setId(id);
			b.setBuyingPrice(buyingPrice);
			b.setDisc1(disc1);
			b.setDisc2(disc2);
			b.setDisc3(disc3);
			b.setDisc4(disc4);
			b.setTaxIncluded(taxIncluded);
			b.setDiscPrice(discPrice);
			
			Measurement selm = coreManager.get(Measurement.class, measurementSelectionId);
			b.setMeasurement(selm);

			Supplier supp = coreManager.get(Supplier.class, supplierId);
			b.setSupplier(supp);
			
			Product prod = coreManager.get(Product.class, id);
			b.setProduct(prod);
						
			try{
				respBuying = coreManager.save(Buying.class, b);
			}catch (Exception e) {
				e.printStackTrace();
				SgwtRestErrorResponse resp = new SgwtRestErrorResponse(-1);
				resp.addError("exception", e.getMessage());
				return resp;								
			}
						
		}
		
		DisplayBuying displayBuying = null;
		
		if(respBuying != null){
			displayBuying = mapper.map(respBuying, DisplayBuying.class);
		}

		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(displayBuying);
		
		return ret;
	}
	
}