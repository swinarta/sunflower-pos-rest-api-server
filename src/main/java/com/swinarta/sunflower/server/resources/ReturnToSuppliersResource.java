package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ReturnToSupplier;
import com.swinarta.sunflower.core.model.Supplier;
import com.swinarta.sunflower.core.model.ReturnToSupplier.Status;
import com.swinarta.sunflower.server.model.DisplaySearchReturnToSupplier;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;
import com.swinarta.sunflower.server.util.ReturnUtil;

public class ReturnToSuppliersResource extends ServerResource{

	private CoreManager coreManager;
	
	private Mapper mapper;
	
	private Properties properties;

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}
	
	@Get("json")
	public SgwtRestFetchResponseBase getRepresent(){
		SgwtRestFetchResponseBase resp = null;
		String text = RequestUtil.getString(getQuery().getValues("searchRetSupp"));
		//String statusStr = RequestUtil.getString(getQuery().getValues("poStatusStr"));
		Integer supplierId = RequestUtil.getInteger(getQuery().getValues("supplierId"));		
		Integer start = RequestUtil.getInteger(getQuery().getValues("_startRow"));
		Integer end = RequestUtil.getInteger(getQuery().getValues("_endRow"));
		
		//Status status = Status.fromString(statusStr);

		ResultList<ReturnToSupplier> list = coreManager.searchReturnToSupplier(text, supplierId, null, null, start, end);
				
		List<DisplaySearchReturnToSupplier> list2 = new ArrayList<DisplaySearchReturnToSupplier>();
		
		for (ReturnToSupplier ret : list) {
			list2.add(mapper.map(ret, DisplaySearchReturnToSupplier.class));
		}
		
		ResultList<DisplaySearchReturnToSupplier> list3 = new ResultList<DisplaySearchReturnToSupplier>(list2);
		
		resp = new SgwtRestFetchResponseBase(new ResultList<DisplaySearchReturnToSupplier>(list3, DisplaySearchReturnToSupplier.class));
		return resp;
	}
	
	@Post("json")
	public SgwtRestResponseBase add(SgwtRequest req){
		Serializable resp = null;
		Date returnDate = new Date();
		Integer supplierId = RequestUtil.getInteger(req.getData().get("supplierId"));
		String remarks = RequestUtil.getString(req.getData().get("remarks"));
		String invNumber = RequestUtil.getString(req.getData().get("invoiceNumber"));
		
		Supplier supplier = coreManager.get(Supplier.class, supplierId);
		Integer storeId = Integer.parseInt(properties.getProperty("curr.store.id"));
		
		ReturnToSupplier retSup = new ReturnToSupplier();
		retSup.setInvoiceNumber(invNumber);
		retSup.setRemarks(remarks);
		retSup.setReturnDate(returnDate);
		retSup.setSupplier(supplier);
		retSup.setStatus(Status.fromString("NEW"));
		retSup.setReturnId("temp");
		retSup.setStoreId(storeId);
				
		try {
			ReturnToSupplier retResp = coreManager.save(ReturnToSupplier.class, retSup);
			retResp.setReturnId(ReturnUtil.constructReturnId(retResp.getReturnDate(), retResp.getSupplier().getId(), retResp.getId()));
			retResp = coreManager.save(ReturnToSupplier.class, retResp);
			
			DisplaySearchReturnToSupplier dret = mapper.map(retResp, DisplaySearchReturnToSupplier.class);
			resp = dret;
			
		} catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;
	}
}
