package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Promo;
import com.swinarta.sunflower.core.model.Promo.PromoType;
import com.swinarta.sunflower.server.model.DisplayPromo;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class PromosResource extends ServerResource {

	protected CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}
	
	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		List<Promo> promoList = coreManager.getCurrentAndFuturePromo();
		List<DisplayPromo> respPromoList = new ArrayList<DisplayPromo>();
		
		for (Promo promo : promoList) {
			respPromoList.add(mapper.map(promo, DisplayPromo.class));
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(new ResultList<DisplayPromo>(respPromoList));
		
		return ret;
	}
	
	@Post("json")
	public SgwtRestResponseBase add(SgwtRequest req){
		Serializable resp = null;
		Date startDate = RequestUtil.getDate(req.getData().get("startDate")); 
		Date endDate = RequestUtil.getDate(req.getData().get("endDate")); 

		String promoTypeStr = RequestUtil.getString(req.getData().get("promoTypeStr"));
		Double promoValue = RequestUtil.getDouble(req.getData().get("promoValue"));
		String description = RequestUtil.getString(req.getData().get("description"));

		Promo promo = new Promo();
		promo.setStartDate(startDate);
		promo.setEndDate(endDate);
		promo.setPromoType(PromoType.fromString(promoTypeStr));
		promo.setPromoValue(promoValue);
		promo.setDescription(description);
		
		try {
			Promo promoResp = coreManager.save(Promo.class, promo);
			resp = promoResp;
		} catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}
		
		return new SgwtRestFetchResponseBase(resp);
	}	

}
