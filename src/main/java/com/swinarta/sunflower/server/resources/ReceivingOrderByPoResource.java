package com.swinarta.sunflower.server.resources;

import java.io.Serializable;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ReceivingOrder;
import com.swinarta.sunflower.server.model.DisplayReceivingOrder;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ReceivingOrderByPoResource extends ServerResource{

	private CoreManager coreManager;
	
	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestFetchResponseBase getRepresent(){
		Serializable resp = null;
		
		Integer poId = RequestUtil.getInteger(getRequestAttributes().get("po"));
		ReceivingOrder ro = coreManager.getReceivingOrderByPO(poId);
		
		if(ro != null){
			DisplayReceivingOrder disp = mapper.map(ro, DisplayReceivingOrder.class);
			resp = disp;
		}
		
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);
		return ret;
		
	}
	
}
