package com.swinarta.sunflower.server.resources;

import java.io.Serializable;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ReturnToSupplier;
import com.swinarta.sunflower.core.model.ReturnToSupplier.Status;
import com.swinarta.sunflower.server.model.DisplaySearchReturnToSupplier;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ReturnToSupplierResource extends ServerResource{

	private CoreManager coreManager;
	
	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}
	
	@Get("json")
	public SgwtRestResponseBase fetch(){
		Serializable resp = null;
		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		ReturnToSupplier ret = coreManager.get(ReturnToSupplier.class, id);
		if(ret != null){
			DisplaySearchReturnToSupplier dret = mapper.map(ret, DisplaySearchReturnToSupplier.class);
			resp = dret;
		}
		return new SgwtRestFetchResponseBase(resp);
		
	}
	
	@Post("json")
	public SgwtRestResponseBase update(SgwtRequest req){
		Serializable resp = null;		
		Integer id = RequestUtil.getInteger(getRequestAttributes().get("id"));
		String invNumber = RequestUtil.getString(req.getData().get("invoiceNumber"));
		String remarks = RequestUtil.getString(req.getData().get("remarks"));
		String status = RequestUtil.getString(req.getData().get("status"));
		Boolean isCompleted = false;
		
		ReturnToSupplier retResp = null;
		ReturnToSupplier ret = coreManager.get(ReturnToSupplier.class, id);			

		Status RetStatus = Status.fromString(status);
		
		if(status != null){
			Status oldStatus = ret.getStatus();
			Status newStatus = Status.fromString(status);
			isCompleted = (!oldStatus.equals(newStatus) && newStatus == Status.COMPLETED);
		}
		
		try {
			if(isCompleted){
				retResp = coreManager.updateCompleteReturnToSupplier(id);
			}else{
				ret.setRemarks(remarks);
				ret.setInvoiceNumber(invNumber);
				ret.setStatus(RetStatus);
				retResp = coreManager.save(ReturnToSupplier.class, ret);
			}
			DisplaySearchReturnToSupplier dret = mapper.map(retResp, DisplaySearchReturnToSupplier.class);
			resp = dret;
		} catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}
		
		return new SgwtRestFetchResponseBase(resp);
	}
}
