package com.swinarta.sunflower.server.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.Promo;
import com.swinarta.sunflower.core.model.PromoDetail;
import com.swinarta.sunflower.server.model.DisplayProductMeasurement;
import com.swinarta.sunflower.server.model.DisplayPromoDetail;
import com.swinarta.sunflower.server.model.SgwtRequest;
import com.swinarta.sunflower.server.model.SgwtRestErrorResponse;
import com.swinarta.sunflower.server.model.SgwtRestFetchResponseBase;
import com.swinarta.sunflower.server.model.SgwtRestResponseBase;
import com.swinarta.sunflower.server.util.RequestUtil;

public class PromoDetailsResource extends ServerResource{

	private CoreManager coreManager;

	private Mapper mapper;
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get("json")
	public SgwtRestResponseBase getRepresent(){
		SgwtRestFetchResponseBase resp = null;
		Integer promoId = RequestUtil.getInteger(getRequestAttributes().get("promoId"));
		List<PromoDetail> details = coreManager.getPromoDetails(promoId);
				
		List<DisplayPromoDetail> resultList = new ArrayList<DisplayPromoDetail>();

		for (PromoDetail promoDetail : details) {
			ProductMeasurement productMeasurement = null;
			if(promoDetail.getProduct().getProductMeasurement() != null && !promoDetail.getProduct().getProductMeasurement().isEmpty()){
				productMeasurement = promoDetail.getProduct().getProductMeasurement().iterator().next();
			}
			
			DisplayPromoDetail det = mapper.map(promoDetail, DisplayPromoDetail.class);

			if(productMeasurement != null){
				det.getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
			}

			resultList.add(det);

		}

		resp = new SgwtRestFetchResponseBase(resultList);
		
		return resp;
		
	}
	
	@Post("json")
	public SgwtRestResponseBase add(SgwtRequest request){
		Integer promoId = RequestUtil.getInteger(request.getData().get("promoId"));
		Integer productId = RequestUtil.getInteger(request.getData().get("productId"));
		Serializable resp = null;
		
		try {
			Promo promo = coreManager.get(Promo.class, promoId);
			Product product = coreManager.get(Product.class, productId);
			
			PromoDetail det = new PromoDetail();
			det.setProduct(product);
			det.setPromo(promo);
			
			PromoDetail promoDetail =  coreManager.save(PromoDetail.class, det);
			promoDetail = coreManager.getPromoDetail(promoDetail.getId());
			
			DisplayPromoDetail dispDet = mapper.map(promoDetail, DisplayPromoDetail.class);
			ProductMeasurement productMeasurement = null;
			if(promoDetail.getProduct().getProductMeasurement() != null && !promoDetail.getProduct().getProductMeasurement().isEmpty()){
				productMeasurement = promoDetail.getProduct().getProductMeasurement().iterator().next();
			}
			if(productMeasurement != null){
				dispDet.getProduct().setProductMeasurement(mapper.map(productMeasurement, DisplayProductMeasurement.class));
			}
			
			resp = dispDet;
		} catch (Exception e) {
			SgwtRestErrorResponse resp1 = new SgwtRestErrorResponse(-1);
			resp1.addError("exception", e.getMessage());
			return resp1;				
		}
				
		SgwtRestFetchResponseBase ret = new SgwtRestFetchResponseBase(resp);		
		return ret;
		
	}
		
}
