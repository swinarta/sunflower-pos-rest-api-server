package com.swinarta.sunflower.server.resources;

import org.restlet.resource.Delete;
import org.restlet.resource.ServerResource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Stock.StockUpdateReason;
import com.swinarta.sunflower.core.model.User;
import com.swinarta.sunflower.server.util.RequestUtil;

public class TransferStockResource extends ServerResource{
	
	private CoreManager coreManager;
				
	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}
	
	@Delete
	public void updateStockOnComplete() throws Exception{
		//when transfer order is updated to complete, it will send this req to target store to update its stock (plus) 
		Integer transferId = RequestUtil.getInteger(getRequestAttributes().get("id"));
		Integer userId = RequestUtil.getInteger(getRequestAttributes().get("userid"));
		String type = RequestUtil.getString(getRequestAttributes().get("type"));
		
		boolean toAdd = true;
		StockUpdateReason reason = StockUpdateReason.TR_IN;
		
		if(type.equalsIgnoreCase("out")){
			toAdd = false;
			reason = StockUpdateReason.TR_OUT;
		}else 	if(type.equalsIgnoreCase("cancelout")){
			toAdd = true;
			reason = StockUpdateReason.TR_OUT_C;
		}
		
		User user = new User();
		user.setId(userId);
		
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user, null);		
		SecurityContextHolder.getContext().setAuthentication(token);
		coreManager.updateStockRemote(transferId, reason, toAdd);
	}
		

}
