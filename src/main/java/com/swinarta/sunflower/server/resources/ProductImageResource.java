package com.swinarta.sunflower.server.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.FileRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;
import org.springframework.orm.ObjectRetrievalFailureException;

import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.ProductImage;
import com.swinarta.sunflower.server.util.RequestUtil;

public class ProductImageResource extends ServerResource{

	private CoreManager coreManager;

	public void setCoreManager(CoreManager coreManager) {
		this.coreManager = coreManager;
	}

	@Get
	public FileRepresentation getRepresent() throws IOException{
		
		Integer id = RequestUtil.getInteger(getRequest().getAttributes().get("id"));
		
		try{
			ProductImage respImage = coreManager.get(ProductImage.class, id);

			File f = File.createTempFile("sunflower_", ".img");
						
			FileOutputStream fos = new FileOutputStream(f);
			
			fos.write(respImage.getImage());
			
			fos.close();
							
			FileRepresentation resp = new FileRepresentation(f, MediaType.IMAGE_ALL);
			
			return resp;
			
		}catch (ObjectRetrievalFailureException e) {
			getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
		}

		return null;
	}

}
