package com.swinarta.sunflower.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SeedInvalidSessionHandler extends SeedMarkerResponseHandler implements InvalidSessionHandler{

	protected final Log logger = LogFactory.getLog(getClass());

	public SeedInvalidSessionHandler(){
        setMarkerSnippet(LOGIN_REQUIRED_MARKER);
    }
	
	public void sessionInvalidated(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		if(logger.isDebugEnabled())
            logger.debug("responded with LOGIN_REQUIRED_MARKER");
        handle(request,response);
	}

}
