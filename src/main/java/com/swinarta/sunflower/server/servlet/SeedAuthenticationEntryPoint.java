package com.swinarta.sunflower.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

public class SeedAuthenticationEntryPoint extends SeedMarkerResponseHandler implements AuthenticationEntryPoint{

	public SeedAuthenticationEntryPoint(){
        setMarkerSnippet(LOGIN_REQUIRED_MARKER);
    }

	public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
		handle(httpServletRequest,httpServletResponse,e);
	}

}