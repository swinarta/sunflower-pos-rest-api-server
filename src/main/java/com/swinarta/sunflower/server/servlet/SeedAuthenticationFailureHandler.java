package com.swinarta.sunflower.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

public class SeedAuthenticationFailureHandler extends SeedMarkerResponseHandler implements AuthenticationFailureHandler{

	protected final Log logger = LogFactory.getLog(getClass());

    public SeedAuthenticationFailureHandler(){
        setMarkerSnippet(LOGIN_REQUIRED_MARKER);
    }

	public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
		if(logger.isDebugEnabled())
            logger.debug("responded with LOGIN_REQUIRED_MARKER");
        handle(httpServletRequest,httpServletResponse,e);
	}

}
