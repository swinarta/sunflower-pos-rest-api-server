package com.swinarta.sunflower.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class SeedAuthenticationSuccessHandler extends SeedMarkerResponseHandler implements AuthenticationSuccessHandler{

	protected final Log logger = LogFactory.getLog(getClass());
	
    public SeedAuthenticationSuccessHandler(){
        setMarkerSnippet(SUCCESS_MARKER);
    }
    
	public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException,	ServletException {
		if(logger.isDebugEnabled())
            logger.debug("responded with SUCCESS_MARKER");
        handle(httpServletRequest,httpServletResponse,authentication);
	}

}
