package com.swinarta.sunflower.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface InvalidSessionHandler {
	void sessionInvalidated(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException;
}
