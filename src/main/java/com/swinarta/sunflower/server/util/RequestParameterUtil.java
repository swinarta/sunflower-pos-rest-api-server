package com.swinarta.sunflower.server.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class RequestParameterUtil {

	public static Map<String, String> decode(String s){
		Map<String,String> retMap = new HashMap<String,String>();
		String params[] = s.split("&");
		
		for (String param : params) {
		       String temp[] = param.split("=");
		       try {
				retMap.put(temp[0], URLDecoder.decode(temp[1], "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		
		return retMap;
	}
	
}
