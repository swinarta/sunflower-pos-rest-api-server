package com.swinarta.sunflower.server.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class RequestUtil {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public static Integer getInteger(Object object){
		
		if(object instanceof Number){
			return ((Number) object).intValue();
		}else if(object instanceof String){
			try{
				return Integer.parseInt((String)object);
			}catch (Exception e) {
			}
		}

		return null;
	}

	public static Double getDouble(Object object){
		
		if(object instanceof Number){
			return ((Number) object).doubleValue();
		}else if(object instanceof String){
			try{
				return Double.parseDouble((String)object);
			}catch (Exception e) {
			}
		}

		return null;
	}

	public static Float getFloat(Object object){
		
		if(object instanceof Number){
			return ((Number) object).floatValue();
		}else if(object instanceof String){
			try{
				return Float.parseFloat((String)object);
			}catch (Exception e) {
			}
		}

		return null;
	}

	public static String getString(Object object){
		
		if(object instanceof String){
			try{
				return ((String)object);
			}catch (Exception e) {
			}
		}

		return null;
	}

	public static Boolean getBoolean(Object object){
		
		if(object instanceof Boolean){
			return (Boolean) object;
		}else if(object instanceof String){
			try{
				return Boolean.parseBoolean((String)object);
			}catch (Exception e) {
			}
		}

		return null;
	}
	
	public static Date getDate(Object object){
		
		if(object instanceof String){
			String str = (String)object;
			String dateStr = StringUtils.substringBefore(str, "T");			
			try {
				return sdf.parse(dateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else if (object instanceof Long){
			Long time = (Long)object;
			return new Date(time);
		}
		
		return null;
		
	}

}
