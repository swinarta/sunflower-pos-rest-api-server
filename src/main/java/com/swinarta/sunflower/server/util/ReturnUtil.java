package com.swinarta.sunflower.server.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class ReturnUtil {

	private static String RET_PREFIX = "T";	
	private static SimpleDateFormat roYearFormat = new SimpleDateFormat("yy");
	private static int SUPPLIER_ID_SIZE = 4;
	private static int RET_ID_SIZE = 5;
	
	public static String constructReturnId(Date retDate, Integer suppId, Integer retId){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(RET_PREFIX);
		buffer.append(roYearFormat.format(retDate));		
		buffer.append(StringUtils.leftPad(suppId.toString(), SUPPLIER_ID_SIZE, '0'));
		buffer.append(StringUtils.leftPad(((Integer)(retId%100000)).toString(), RET_ID_SIZE, '0'));
		
		return buffer.toString();
		
	}
	
}
