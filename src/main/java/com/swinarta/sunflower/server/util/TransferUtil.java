package com.swinarta.sunflower.server.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class TransferUtil {

	private static String PREFIX = "C";	
	private static SimpleDateFormat yearFormat = new SimpleDateFormat("yy");
	private static int ID_SIZE = 5;
	
	public static String constructCode(Date date, Integer storeFromId, Integer storeToId, Integer tid){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(PREFIX);
		buffer.append(yearFormat.format(date));		
		buffer.append(StringUtils.leftPad(String.valueOf(storeFromId), 2, '0'));
		buffer.append(StringUtils.leftPad(String.valueOf(storeToId), 2, '0'));
		buffer.append(StringUtils.leftPad(((Integer)(tid%100000)).toString(), ID_SIZE, '0'));
		
		return buffer.toString();
		
	}
	
}
