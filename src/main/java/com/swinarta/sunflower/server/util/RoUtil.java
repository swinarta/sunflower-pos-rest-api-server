package com.swinarta.sunflower.server.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class RoUtil {

	private static String RO_PREFIX = "R";	
	private static SimpleDateFormat roYearFormat = new SimpleDateFormat("yy");
	private static int SUPPLIER_ID_SIZE = 4;
	private static int RO_ID_SIZE = 5;
	
	public static String constructRoId(Date roDate, Integer suppId, Integer poId){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(RO_PREFIX);
		buffer.append(roYearFormat.format(roDate));		
		buffer.append(StringUtils.leftPad(suppId.toString(), SUPPLIER_ID_SIZE, '0'));
		buffer.append(StringUtils.leftPad(((Integer)(poId%100000)).toString(), RO_ID_SIZE, '0'));
		
		return buffer.toString();
		
	}
	
}
