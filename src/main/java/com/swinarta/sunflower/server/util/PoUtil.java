package com.swinarta.sunflower.server.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class PoUtil {

	private static String PO_PREFIX = "P";	
	private static SimpleDateFormat poYearFormat = new SimpleDateFormat("yy");
	private static int SUPPLIER_ID_SIZE = 4;
	private static int PO_ID_SIZE = 5;
	
	public static String constructPoId(Date poDate, Integer suppId, Integer poId){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(PO_PREFIX);
		buffer.append(poYearFormat.format(poDate));		
		buffer.append(StringUtils.leftPad(suppId.toString(), SUPPLIER_ID_SIZE, '0'));
		buffer.append(StringUtils.leftPad(((Integer)(poId%100000)).toString(), PO_ID_SIZE, '0'));
		
		return buffer.toString();
		
	}
	
}
